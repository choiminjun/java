/**
 Program Insertion Sort.
 Author : Choi Min Jun.
 Email Address: choiminjun0720@gmail.com
 Programming Projects 7 - 5.
 Last Changed : 09/05/2014.
 */
public class InsertionSortDemo {
	static int arr[] = new int[40];
	public static void main(String[] args){
		setValue();
		InsertionSort s = new InsertionSort();
		s.display(arr);
		arr = s.sort(arr);
		s.display(arr);
	}
	/**
	 * setting array using random method.
	 */
	public static void setValue(){
		for(int i = 0 ; i < arr.length ; i++){
			arr[i] = (int)(Math.random() *21);	//put random number;
		}
	}
}
