import java.util.Scanner;
public class TempConverter {
	public static void main(String[] args){
		String choice;
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Put temperature : ");
		double temperature = keyboard.nextDouble();
		System.out.println("Is this for Celsius or Fahrenheit?");
		choice = keyboard.next();
		converter(choice  , temperature);
	}
	public static void converter(String c , double temp){
		double result = 0;
		if(c.equalsIgnoreCase("C")){
			result =( 5 * ( temp - 32) ) / 9;
			System.out.println("Converting Celsius to Fahrenheit..");
			System.out.println( result);
		}
		else if(c.equalsIgnoreCase("F")){
			result = (9 * temp) / 5  + 32;
			System.out.println("Converting Fahrenheit to Celsius..");
			System.out.println( result);
		}
		else
			System.out.println("ERROR");
	}
	
}
