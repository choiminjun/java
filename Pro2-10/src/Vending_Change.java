/**
 Program calculates vending machines change.
 (Input is only one dollar, from 25cents to 100 which increases per 5 cents).
 Author : Choi Min Jun.
 Email Address: choiminjun0720@gmail.com
 Programming Projects 2 - 10.
 Last Changed : 15/03/2014.
 */
import java.util.Scanner;
public class Vending_Change {
	public static void main(String[] args){
		int item_price;
		int remainder;		//100 - item_price;
		int quarters, dimes, nickel;
		quarters = dimes = nickel = 0 ; //initializing 0.
		System.out.println("Enter price of item");
		System.out.print("(from 25 cents to a dollar, in 5-cent increments): ");
		Scanner keyboard = new Scanner(System.in);
		item_price = keyboard.nextInt();
		remainder = 100 - item_price;	//calculating remainder.
		quarters = remainder / 25;
		remainder = remainder - (quarters * 25); 	//new remainder.
		dimes = remainder / 10;
		remainder = remainder - (dimes * 10);
		nickel = remainder / 5;
		System.out.print("You bought an item for 45 cents and gave me a dollar,\nso your change is\n");
		System.out.print(quarters + " quarters,\n" + dimes + " dimes, and\n" + nickel + " nickel.");
	}
}
