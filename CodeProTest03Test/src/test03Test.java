import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>test03Test</code> contains tests for the class <code>{@link test03}</code>.
 *
 * @generatedBy CodePro at 14. 5. 27 오후 8:03
 * @author MINJUN
 * @version $Revision: 1.0 $
 */
public class test03Test {
	/**
	 * Run the int[] sort(int[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 5. 27 오후 8:03
	 */
	@Test
	public void testSort_1()
		throws Exception {
		test03 fixture = new test03();
		int[] arr = new int[] {4, 1};

		int[] result = fixture.sort(arr);

		// add additional test code here
		assertNotNull(result);
		assertEquals(2, result.length);
		assertEquals(1, result[0]);
		assertEquals(4, result[1]);
	}

	/**
	 * Run the int[] sort(int[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 5. 27 오후 8:03
	 */
	@Test
	public void testSort_2()
		throws Exception {
		test03 fixture = new test03();
		int[] arr = new int[] {1, 5};

		int[] result = fixture.sort(arr);

		// add additional test code here
		assertNotNull(result);
		assertEquals(2, result.length);
		assertEquals(1, result[0]);
		assertEquals(5, result[1]);
	}

	/**
	 * Run the int[] sort(int[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 5. 27 오후 8:03
	 */
	@Test
	public void testSort_3()
		throws Exception {
		test03 fixture = new test03();
		int[] arr = new int[] {2, 1};

		int[] result = fixture.sort(arr);

		// add additional test code here
		assertNotNull(result);
		assertEquals(2, result.length);
		assertEquals(1, result[0]);
		assertEquals(2, result[1]);
	}

	/**
	 * Run the int[] sort(int[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 5. 27 오후 8:03
	 */
	@Test
	public void testSort_4()
		throws Exception {
		test03 fixture = new test03();
		int[] arr = new int[] {1, 3};

		int[] result = fixture.sort(arr);

		// add additional test code here
		assertNotNull(result);
		assertEquals(2, result.length);
		assertEquals(1, result[0]);
		assertEquals(3, result[1]);
	}

	/**
	 * Run the int[] sort(int[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 5. 27 오후 8:03
	 */
	@Test
	public void testSort_5()
		throws Exception {
		test03 fixture = new test03();
		int[] arr = new int[] {};

		int[] result = fixture.sort(arr);

		// add additional test code here
		assertNotNull(result);
		assertEquals(0, result.length);
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 14. 5. 27 오후 8:03
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 14. 5. 27 오후 8:03
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 14. 5. 27 오후 8:03
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(test03Test.class);
	}
}