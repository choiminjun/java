/**
 * Program:9-2.
 *  Author:Choi Min Jun 
 *  Last Changed: 31/05/2014
 * @author MINJUN
 */
import java.util.Scanner;

//throwing Exception
public class TimeFormat extends Exception {
	/**
	 * Class Exception Handling..
	 * 
	 * @author MINJUN
	 * 
	 */

	public class TimeFormatException extends Exception {
		public TimeFormatException() {
			super("Error in Format");
		}

		public TimeFormatException(String msg) {
			super("There is no time such as " + msg);
		}
	}

	/**
	 * Class main
	 */

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		String check = null;
		do {
			try {
				TimeFormat s = new TimeFormat();
				s.doIt();
			} catch (TimeFormatException e) {
				System.out.println(e.getMessage());
			}
			System.out.println("Again?");
			check = keyboard.nextLine();
		} while (check.equalsIgnoreCase("y"));
	}

	private void doIt() throws TimeFormatException {
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Enter time in 24-hour notation: ");
		String time = keyboard.nextLine();
		int hour = (Integer.parseInt(time.substring(0, 2)));
		int minute = (Integer.parseInt(time.substring(3, 5)));
		if (hour <= 24 && minute <= 60) {
			if (hour < 12) {
				time = time + " " + "AM";
			} else { // if hour is larger than 12(PM).
				time = (hour - 12) + ":" + time.substring(3, 5) + " PM";
			}
		} else
			throw new TimeFormatException(time);

		System.out.println("That is the same as");
		System.out.println(time);
	}
}