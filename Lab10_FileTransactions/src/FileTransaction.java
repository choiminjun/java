import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.util.Scanner;

public class FileTransaction {
	public static void main(String[] args) {
		String fileName = "Transactions.txt";
		
		BufferedReader in = null;
		double tot = 0;
		int count = 0;
		try {
			in = new BufferedReader(new FileReader(fileName));
			String l = null;
//			Scanner inn = new Scanner(new FileInputStream(fileName));
			while ((l = in.readLine()) != null) {
				String[] arr = l.split(",");
				count++;
				if (count == 1)
					continue;
				System.out
						.println("Sold " + arr[1] + " of " + arr[3] + "(SKU: "
								+ arr[0] + ") " + "at $" + arr[2] + " each.");
				tot += Double.parseDouble(arr[0]) * Double.parseDouble(arr[2]);

			}
		} catch (Exception e) {
			;
		}finally{
			System.out.println(tot);
		}
	}
}
