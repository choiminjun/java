/**
 * Program:8-11.
 *  Author:Choi Min Jun 
 *  Last Changed: 31/05/2014
 * @author MINJUN
 */
public class StudentDemo {
	public static void main(String[] args) {
		Student[] student = new Student[(int) (Math.random() * 100)];
		/**
		 * Assing name and studentId randomically.
		 *and set those by using constructor.
		 */
		for (int i = 0; i < student.length; i++){
			String randomStudentName = makeRandomName();
			int randomStudentID = (int)(Math.random()*3000);
			student[i] = new Student(randomStudentName,randomStudentID); // allocate Student in each array.
		}
		/**
		 * Sort stuID by using bubble sort.
		 */
		for (int i = 0; i < student.length; i++) {
			for (int j = 0; j < student.length; j++) {
				if(j+1 == student.length)
					continue;	//for array out of bounds.
				if (student[j].compareTo(student[j + 1]) > 0) {
					Student temp = null;
					temp = student[j];
					student[j] = student[j + 1];
					student[j + 1] = temp;
				}
			}
		}
		
		
		
		for(int i =0; i < student.length; i++)
			System.out.println(student[i].getStudentNumber());
		
		

		/**
		 * Sort stuName by using bubble sort.
		 */
		for (int i = 0; i < student.length; i++) {
			for (int j = 0; j < student.length; j++) {
				if(j+1 == student.length)
					continue;	//for array out of bounds.
				String name = student[j+1].getName();
				if (student[j].compareTo(name) > 0) {
					Student temp = null;
					temp = student[j];
					student[j] = student[j + 1];
					student[j + 1] = temp;
				}
			}
		}
		for(int i =0; i< student.length; i++)
			System.out.println(student[i].getName());
		
	}
	
	private static String makeRandomName(){
		char[] arr = new char[5];
		for(int i = 0; i < arr.length; i++){
			arr[i] = (char)((int)(Math.random()*25) + 65);
		}
		/**
		 * Call static method in student class for
		 * making random name.
		 */
		String randomName = Student.toString(arr);
		return randomName;
	}
}
