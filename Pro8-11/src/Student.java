public class Student extends Person implements Comparable {
	private int studentNumber;

	public Student() {
		super();
		studentNumber = 0;
	}

	public Student(String initialName, int initialStudentNumber) {
		super(initialName);
		studentNumber = initialStudentNumber;
	}

	public void reset(String newName, int newStudentNumber) {
		setName(newName);
		studentNumber = newStudentNumber;
	}

	public int getStudentNumber() {
		return studentNumber;
	}

	public void setStudentNumber(int newStudentNumber) {
		studentNumber = newStudentNumber;
	}

	public void writeOutput() {
		System.out.println("Name: " + getName());
		System.out.println("Student Number: " + studentNumber);
	}

	public boolean equals(Student otherStudent) {
		return this.hasSameName(otherStudent)
				&& (this.studentNumber == otherStudent.studentNumber);
	}

	/**
	 * Overriding method. Return subtraction of two student's ID. returning
	 * bigger than 0 means left's stunumber is bigger than right side.
	 */
	@Override
	public int compareTo(Student otherStudent) {
		return this.studentNumber - otherStudent.getStudentNumber();
	}

	public int compareTo(String otherStudentName) {
		return this.getName().compareTo(otherStudentName);
	}

	public static String toString(char[] arr) {
		String randomName = "";
		for (int i = 0; i < arr.length; i++) {
			randomName += arr[i];
		}
		return randomName;
	}

	@Override
	public int compareTo(Object o) {
		return this.studentNumber - ((Student) o).getStudentNumber();
	}
}
