
public class Person {
	private String name;
	public Person(){
		name = "No name yet";
	}
	public Person(String initialName){
		name = initialName;
	}
	public void setName(String newName){
		name = newName;
	}
	public String getName(){
		return name;
	}
	public void writeOutput(){
		System.out.println("Name: " + name);
	}
	public boolean hasSameName(Person otherPerson){
		return this.name.equalsIgnoreCase(otherPerson.name);
	}
	public int compareTo(Student otherStudent) {
		return 0;
	}
	public int compareTo(Student otherStudent, int Case) {
		// TODO Auto-generated method stub
		return 0;
	}
	public int compareTo(Student otherStudent, String condition) {
		// TODO Auto-generated method stub
		return 0;
	}
}
