/**
 Program to display each number of four-digit numbers.
 Author : Choi Min Jun.
 Email Address: choiminjun0720@gmail.com
 Programming Projects 2 - 4.
 Last Changed : 15/03/2014.
 */
import java.util.Scanner;
public class DisplayNum {
	public static void main(String[] args){
		int num;
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Input four-digit numbers");
		num = keyboard.nextInt();
		System.out.println(num/1000);	//divide
		System.out.println( (num%1000) / 100 );	//remainder.
		System.out.println( ( ( num % 1000) % 100 ) / 10 );
		System.out.println( ( ( num % 1000) % 100 ) % 10 );
	}
	
}
