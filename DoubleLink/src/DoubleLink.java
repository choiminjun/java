
public class DoubleLink {
	
	Node head, tail;
	
	public DoubleLink(){
		head = null;
		tail = null;
	}
	public void showList(){
		Node head_position = head;
		Node tail_position = tail;
		System.out.println("Printing from head");
		while(head_position != null){
			System.out.print(head_position.data+"   ");
			head_position = head_position.R_next;
		}
		System.out.println();
		System.out.println("Printing from tail");
		while(tail_position != null){
			System.out.print(tail_position.data+ "    ");
			tail_position = tail_position.L_next;;
		}
	}

	public void addANode(String data){
		Node newNode = new Node(data);
		if(head == null	){
			head = newNode;
			tail = newNode;
		}
		else{
			Node prev_ptr = tail;
			prev_ptr.R_next = newNode;
			newNode.L_next = prev_ptr;
			tail = tail.R_next;
			
		}
	}
	
	private class Node{
		String data;
		Node L_next;
		Node R_next;
		
		public Node(){
			this.data = null;
			this.L_next = null;
			this.R_next = null;
		}
		public Node(String data){
			this.data = data;
			this.L_next = null;
			this.R_next = null;
		}
	}
}
