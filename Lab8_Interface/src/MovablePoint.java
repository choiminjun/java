
public class MovablePoint implements Movable{
	public int x = 0;
	public int y = 0;
	public MovablePoint(int x, int y){
		this.x = x;
		this.y = y;
	}
	public void moveUp() {
		this.y++;
	}

	@Override
	public void moveDown() {
		// TODO Auto-generated method stub
		this.y--;
	}

	@Override
	public void moveLeft() {
		// TODO Auto-generated method stub
		this.x--;
	}

	@Override
	public void moveRight() {
		// TODO Auto-generated method stub
		this.x++;
	}
	public String toString(){
		String str = "(" + x + "," +y +")";
		return str;
	}
	
}
