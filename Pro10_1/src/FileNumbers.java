/**
 * Program:To FileIO.
 *  Author:Choi Min Jun 
 *  Last Changed: 29/05/2014
 * @author MINJUN
 */
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;


public class FileNumbers {
	public static void main(String[] args){
		String filename = null;
		Scanner in = null;
		PrintWriter out = null;
		int[] arr = new int[(int)(Math.random() * 200)];
		
		System.out.println("Put file names");
		Scanner keyboard = new Scanner(System.in);
		//filename = keyboard.nextLine();
		filename = args[0];
		try {
			out = new PrintWriter(new File(filename));	//this is for making random number in txt file.
			for(int i = 0; i < arr.length ; i++)
				out.println((int)(Math.random() * 200 ));
			out.close();
			
			in = new Scanner(new File(filename));
			int count = 0;
			while( in.hasNextLine() ){
				String line = in.nextLine();
				arr[count++] = Integer.parseInt(line);
			}
			in.close();
			System.out.println("MIN: " + getmax(arr));
			System.out.println("MAX: " + getmin(arr));
			System.out.println("AVR: " + getavr(arr));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private static int getmax(int[] find_max){
		int max = find_max[0];
		for(int i = 1; i < find_max.length ; i++){
			if(max < find_max[i])
				max = find_max[i];
		}
		return max;
	}
	private static int getmin(int[] find_min){
		int min = find_min[0];
		for(int i = 1; i < find_min.length ; i++){
			if(min > find_min[i])
				min = find_min[i];
		}
		return min;
	}
	private static double getavr(int[] get_avr){
		int tot = 0;
		for(int i = 0; i < get_avr.length; i++)
			tot += get_avr[i];
		
		return (double)tot / (double)get_avr.length; 
	}
}
