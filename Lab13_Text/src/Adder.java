import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import java.awt.Color;
import java.awt.Container;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
/**
GUI for totaling a series of numbers.
*/
public class Adder extends JFrame implements ActionListener
{	public char before_oper =' ';
    public int result = 0;
	public static final int WIDTH = 400;
    public static final int HEIGHT = 200;
    private JTextField inOutField;
    private double sum = 0;
    public Adder ()
    {
        setTitle ("Adding Machine");
        addWindowListener (new WindowDestroyer ());
        setSize (WIDTH, HEIGHT);
        Container contentPane = getContentPane ();
        contentPane.setLayout (new BorderLayout ());
        JPanel buttonPanel = new JPanel ();
        buttonPanel.setBackground (Color.GRAY);
        buttonPanel.setLayout (new FlowLayout ());
       
        JButton oneButton = new JButton ("1");
        oneButton.addActionListener (this);
        buttonPanel.add (oneButton);
        JButton twoButton = new JButton ("2");
        twoButton.addActionListener (this);
        buttonPanel.add (twoButton);
        JButton threeButton = new JButton ("3");
        threeButton.addActionListener (this);
        buttonPanel.add (threeButton);
        JButton fourButton = new JButton ("4");
        fourButton.addActionListener (this);
        buttonPanel.add (fourButton);
        JButton fiveButton = new JButton ("5");
        fiveButton.addActionListener (this);
        buttonPanel.add (fiveButton);
        JButton sixButton = new JButton ("6");
        sixButton.addActionListener (this);
        buttonPanel.add (sixButton);
        JButton sevenButton = new JButton ("7");
        sevenButton.addActionListener (this);
        buttonPanel.add (sevenButton);
        JButton eightButton = new JButton ("8");
        eightButton.addActionListener (this);
        buttonPanel.add (eightButton);
        JButton nineButton = new JButton ("9");
        nineButton.addActionListener (this);
        buttonPanel.add (nineButton);
        
        
        //operator..
        JPanel operatorPanel = new JPanel ();
        operatorPanel.setBackground (Color.PINK);
        operatorPanel.setLayout (new FlowLayout ());
        JButton addButton = new JButton ("+");
        addButton.addActionListener (this);
        operatorPanel.add (addButton);
        JButton subButton = new JButton ("-");
        subButton.addActionListener (this);
        operatorPanel.add (subButton);
        JButton divButton = new JButton ("/");
        divButton.addActionListener (this);
        operatorPanel.add (divButton);
        JButton multiButton = new JButton ("*");
        multiButton.addActionListener (this);
        operatorPanel.add (multiButton);
        JButton calButton = new JButton ("=");
        calButton.addActionListener (this);
        operatorPanel.add (calButton);
        JButton cancelButton = new JButton ("C");
        cancelButton.addActionListener (this);
        operatorPanel.add (cancelButton);
        
        
        contentPane.add (buttonPanel, BorderLayout.CENTER);
        contentPane.add (operatorPanel, BorderLayout.SOUTH);
        JPanel textPanel = new JPanel ();
        textPanel.setBackground (Color.BLUE);
        textPanel.setLayout (new FlowLayout ());
        inOutField = new JTextField ("Numbers go here.", 30);
        inOutField.setBackground (Color.WHITE);
        textPanel.add (inOutField);
        contentPane.add (textPanel, BorderLayout.NORTH);
    }


    public void actionPerformed (ActionEvent e)
    {
        if (e.getActionCommand ().equals ("1") || e.getActionCommand ().equals ("2") ||e.getActionCommand ().equals ("3")||e.getActionCommand ().equals ("4")
        		||e.getActionCommand ().equals ("5")||e.getActionCommand ().equals ("6")||e.getActionCommand ().equals ("7")||e.getActionCommand ().equals ("8")||e.getActionCommand ().equals ("9"))              
        {    
        	String input = e.getActionCommand().toString();
        	inOutField.setText(input);
            //sum = sum + stringToDouble (inOutField.getText ());
          //  inOutField.setText (Double.toString (sum));        
        }                                                      
        else if (e.getActionCommand ().equals ("+"))       
        {             
        	//System.out.println(result);
        	before_oper = '+';
                 result += Integer.parseInt(inOutField.getText().toString());                  
        }   
        else if (e.getActionCommand ().equals ("="))       
        {          
        	if(before_oper=='+')
        		result +=  Integer.parseInt(inOutField.getText().toString());
        	inOutField.setText(Integer.toString(result));    
        	// result += Integer.parseInt(e.getActionCommand().toString());                  
        }  
        else if (e.getActionCommand ().equals ("c"))       
        {          
        	if(before_oper=='+')
        		result +=  Integer.parseInt(inOutField.getText().toString());
        	inOutField.setText(Integer.toString(result));    
        	// result += Integer.parseInt(e.getActionCommand().toString());                  
        }  
        else                                                   
            inOutField.setText ("Error in adder code.");           
    }


    private static double stringToDouble (String stringObject)
    {                                                         
        return Double.parseDouble (stringObject.trim ());     
    }                                                         


    public static void main (String [] args)
    {
        Adder guiAdder = new Adder ();
        guiAdder.setVisible (true);
    }
}