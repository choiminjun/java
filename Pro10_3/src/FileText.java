/**
 * Program:To change line.
 *  Author:Choi Min Jun 
 *  Last Changed: 02/05/2014
 * @author MINJUN
 */
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;


public class FileText {
	public static void main(String[] args) throws IOException{
		String line = null;
		String inputFile = null;
		String outputFile =null;
		PrintWriter out = null;
		BufferedReader in = null;
		int index = 0;
		int space_count = 0;	//this is used for counting space wheather there are more than two spaces.
		char[] arr = null;
		
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Put input file name : ");
		inputFile = keyboard.nextLine();
		System.out.println("Put output file name : ");
		outputFile = keyboard.nextLine();
		
		try{
			in = new BufferedReader(new FileReader(new File(inputFile)));
			out = new PrintWriter(new FileWriter(new File(outputFile)));
			
			while( (line= in.readLine()) != null){
				if(line.contains("  ") || line.contains(".") || line.contains(",") || line.contains("?") || line.contains("!")){
					line = changeLineSpace(line);		//call change method.
					line = changeLinePuctuation(line);
					out.println(line);
				}
				else{
					out.println(line);
				}
			}
		//close reader, printwriter. 	
		//in.close();
		//out.close();
			
		}catch(Exception e){
			e.printStackTrace();
		}
		finally {
            if (in != null) {
                in.close();
            }
            if (out != null) {
                out.close();
            }
		}
	}

	private static String changeLinePuctuation(String line) {
		//int index = 0;
		int reflection_count = 0;
		//while(line.charAt(index) != '\n'){
		for(int index =0 ; index < line.length(); index++){
		if(line.charAt(index) == '.'){
			int ascii = (int)line.charAt(index + 1);
			if(ascii >= 97 && ascii<= 122 ){
				ascii -= 32;
				line = line.substring(0, index +1) + (char)ascii + line.substring(index+2,line.length());
				System.out.println(line); //for testing..
			}
			reflection_count++;
		}
		
		if(line.charAt(index) == ',' || line.charAt(index) == '?' || line.charAt(index) == '!'){
			int ascii = (int)line.charAt(index + 1);
			if(ascii >= 65 && ascii <=122){		//This means that it need space between symbol mark and word.
				line = line.substring(0, index + 1) + " " + line.substring(index+1, line.length());
				System.out.println(line); //for testing..
			}

			reflection_count++;
		}
		}
		return line;
	}

	private static String changeLineSpace(String line) {
		int space_count = 0;	//this is used for checking wheather there is two or more spaces in String.
		//int index = 0;
		int reflection_count = 0;		//for reflection of variable changes.
		//while(line.charAt(index) != '\n'){
		for(int index =0; index < line.length(); index++){
		//This condition prohibits line from out of bounds.
			if(index + 1 == line.length())
				return line;
			
			
			if(line.charAt(index) == ' '){
				int prev_index = index;
				while(line.charAt(index) == ' '){
					space_count++;
					index++;
				}
				if(space_count >=2){	//if line's space needs to be changed to single space.
					line = line.substring(0, prev_index) +" " + line.substring(index, (line.length()));
				}
			}
			
		
			space_count = 0;
			//index++;
		}
		return line;		//return line.
	}
}
