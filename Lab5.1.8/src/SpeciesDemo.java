public class SpeciesDemo {
	public static void main(String[] args) {
		
		Species s1 = new Species();
		Species s2 = new Species();

		s1.setSpecies("minjun", 20, 30);
		s2.setSpecies("minjun", 30, 40);
		s1 = s2;
		
		if (s1.equals(s2) == true)
			System.out.println("They are same");
		else
			System.out.println("Ther are not same");
		
		if(s1 == s2)
			System.out.println("Address are same");
		else
			System.out.println("Address are not same");
	}
}
