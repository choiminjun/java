/**
 * Author : Choi Min Jun 
   Pro : 6-5.
 * Last Changed : April. 28. 2014
 */
public class PetDemo {
	public static void main(String[] args) {
		String[][] arr = new String[10][10];
		Pet s1 = new Pet();
		Pet s2 = new Pet();
		Pet s3 = new Pet();
		Pet s4 = new Pet();
		Pet s5 = new Pet();
		/**
		 * Mutator by using method.
		 */
		s1.setPet("A", 20, 50.6);
		s2.setPet("B", 40, 50.2);
		s3.setPet("C", 5, 30.6);
		s4.setPet("D", 44, 22.1);
		s5.setPet("E", 33, 44.2);
		//print out current info.
		System.out.println("Name: " + s1.getName() + " Age: " + s1.getAge()
				+ " Weight: " + s1.getWeight());

		System.out.println("Name: " + s2.getName() + " Age: " + s2.getAge()
				+ " Weight: " + s2.getWeight());

		System.out.println("Name: " + s3.getName() + " Age: " + s3.getAge()
				+ " Weight: " + s3.getWeight());

		System.out.println("Name: " + s4.getName() + " Age: " + s4.getAge()
				+ " Weight: " + s4.getWeight());

		System.out.println("Name: " + s5.getName() + " Age: " + s5.getAge()
				+ " Weight: " + s5.getWeight());
		
		//put name in String array.
		arr[0][0] = s1.getName();
		arr[1][0] = s2.getName(); 
		arr[2][0] = s3.getName();
		arr[3][0] = s4.getName();
		arr[4][0] = s5.getName();
		
		//put age in String array.
		arr[0][1] = Integer.toString(s1.getAge());
		arr[1][1] = Integer.toString(s2.getAge());
		arr[2][1] = Integer.toString(s3.getAge());
		arr[3][1] = Integer.toString(s4.getAge());
		arr[4][1] = Integer.toString(s5.getAge());
		
		//put weight in String array.
		arr[0][2] = Double.toString(s1.getWeight());
		arr[1][2] = Double.toString(s2.getWeight());
		arr[2][2] = Double.toString(s3.getWeight());
		arr[3][2] = Double.toString(s4.getWeight());
		arr[4][2] = Double.toString(s5.getWeight());
		
		
		//get youngest, oldest pet
		int min = Integer.parseInt(arr[0][1]);
		int min_index = 0;
		for(int i = 1 ; i < 5 ; i++){
			if(min > Integer.parseInt(arr[i][1])){
				min = Integer.parseInt(arr[i][1]);
				min_index = i;
			}
		}
		System.out.println("The Youngest Pet is " + arr[min_index][0]);
		
		int max = Integer.parseInt(arr[0][1]);
		int max_index = 0;
		for(int i = 1 ; i < 5 ; i++){
			if(max < Integer.parseInt(arr[i][1])){
				max = Integer.parseInt(arr[i][1]);
				max_index = i;
			}
		}
		System.out.println("The Oldest Pet is " + arr[max_index][0]);
		
		//get smallest/largest Pet.
		double min_weight = Double.parseDouble(arr[0][2]);
		min_index = 0;
		for(int i = 1 ; i < 5 ; i++){
			if(min_weight < Double.parseDouble(arr[i][1])){
				min_weight = Double.parseDouble(arr[i][1]);
				min_index = i;
			}
		}
		System.out.println("The Smallest Pet is " + arr[min_index][0]);
		
		double max_weight = Double.parseDouble(arr[0][1]);
		max_index = 0;
		for(int i = 1 ; i < 5 ; i++){
			if(max_weight < Double.parseDouble(arr[i][1])){
				max_weight = Double.parseDouble(arr[i][1]);
				max_index = i;
			}
		}
		System.out.println("The Largest Pet is " + arr[max_index][0]);
		
	//average weight of five pet , oldest , youngest,small/largest.
	}

	
}
