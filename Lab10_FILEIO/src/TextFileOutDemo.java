import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class TextFileOutDemo {
	public static void main(String[] args) {
		String fileName = "out.txt";
		String duplicatefileName = "duplicate.txt";
		String copyfileName = "copy.txt";
		PrintWriter outputStream = null;
		PrintWriter outputStream2 = null;
		Scanner inputStream = null;
		BufferedReader in = null;
		Scanner keyboard = new Scanner(System.in);
		/**
		try {
			outputStream = new PrintWriter(fileName);	// == new PrintWriter(new FileOutputstream(fileName,true));
			for (int i = 0; i < 3; i++) {
				String str = keyboard.nextLine();
				outputStream.println(i + " " + str);
			}
		} catch (FileNotFoundException e) {
			System.out.println("Error opening the file " + fileName);
			System.exit(0);
		}
		outputStream.close();
	*/
		try{
			in = new BufferedReader(new FileReader("out2.txt"));
			outputStream2 = new PrintWriter(new FileOutputStream(copyfileName,false));

			inputStream = new Scanner(new FileInputStream(fileName));
			outputStream = new PrintWriter(new FileOutputStream(duplicatefileName,true));

			/**
			 * Buffered Reader �� file�� �� �ݾ���� ��. buffer�� ������ �������� ���ִ� ��.
			 */
			String l = null;
			while((l = in.readLine()) != null ){
				outputStream2.println(l);
			}
			
			/**
			 * Using Scanner
			 */
			while(inputStream.hasNextLine()){
				String line = inputStream.nextLine();
				outputStream.println(line);
			}
			
		} catch (FileNotFoundException e) {
			System.out.println("Error opening the file " + fileName);
			System.exit(0);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
		outputStream.close();
		inputStream.close();
		outputStream2.close();
		}
	}
	
}
