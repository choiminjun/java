import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>Save2Test</code> contains tests for the class <code>{@link Save2}</code>.
 *
 * @generatedBy CodePro at 14. 6. 10 오후 4:27
 * @author MINJUN
 * @version $Revision: 1.0 $
 */
public class Save2Test {
	/**
	 * Run the Save2(String,String,String,Integer,Integer,Integer,String,Integer,String) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testSave2_1()
		throws Exception {
		String sFrom = "";
		String sTo = "";
		String sClass = "";
		Integer iAdult = new Integer(1);
		Integer iChildren = new Integer(1);
		Integer iInfant = new Integer(1);
		String sBookingDate = "";
		Integer iPrice = new Integer(1);
		String sTime = "";

		Save2 result = new Save2(sFrom, sTo, sClass, iAdult, iChildren, iInfant, sBookingDate, iPrice, sTime);

		// add additional test code here
		assertNotNull(result);
		assertEquals("   1 1 1  1 ", result.toString());
	}

	/**
	 * Run the String toString() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testToString_1()
		throws Exception {
		Save2 fixture = new Save2("", "", "", new Integer(1), new Integer(1), new Integer(1), "", new Integer(1), "");

		String result = fixture.toString();

		// add additional test code here
		assertEquals("   1 1 1  1 ", result);
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(Save2Test.class);
	}
}