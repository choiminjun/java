

import org.junit.runner.JUnitCore;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * The class <code>TestAll</code> builds a suite that can be used to run all
 * of the tests within its package as well as within any subpackages of its
 * package.
 *
 * @generatedBy CodePro at 14. 6. 10 ���� 4:27
 * @author MINJUN
 * @version $Revision: 1.0 $
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
	MakeDBTest.class,
	buttonLoginTest.class,
	mouse2Test.class,
	Save1Test.class,
	mouse3Test.class,
	ExitListenerTest.class,
	WindowUtilitiesTest.class,
	button2Test.class,
	Save2Test.class,
	button3Test.class,
	mouse1Test.class,
	Server_sqlHelperTest.class,
	buttonEnrollTest.class,
})
public class TestAll {

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 14. 6. 10 ���� 4:27
	 */
	public static void main(String[] args) {
		JUnitCore.runClasses(new Class[] { TestAll.class });
	}
}