import java.awt.event.ActionEvent;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>buttonEnrollTest</code> contains tests for the class <code>{@link buttonEnroll}</code>.
 *
 * @generatedBy CodePro at 14. 6. 10 오후 4:27
 * @author MINJUN
 * @version $Revision: 1.0 $
 */
public class buttonEnrollTest {
	/**
	 * Run the buttonEnroll(LoginPage) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testButtonEnroll_1()
		throws Exception {
		LoginPage type = new LoginPage();

		buttonEnroll result = new buttonEnroll(type);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfo(Server_sqlHelper.java:41)
		//       at LoginPage.setFlightData(LoginPage.java:74)
		//       at LoginPage.<init>(LoginPage.java:120)
		assertNotNull(result);
	}

	/**
	 * Run the buttonEnroll(LoginPage) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testButtonEnroll_2()
		throws Exception {
		LoginPage type = new LoginPage();

		buttonEnroll result = new buttonEnroll(type);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfo(Server_sqlHelper.java:41)
		//       at LoginPage.setFlightData(LoginPage.java:74)
		//       at LoginPage.<init>(LoginPage.java:120)
		assertNotNull(result);
	}

	/**
	 * Run the void actionPerformed(ActionEvent) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testActionPerformed_1()
		throws Exception {
		buttonEnroll fixture = new buttonEnroll(new LoginPage());
		fixture.labelNewPW = new JLabel();
		fixture.labelNewID = new JLabel();
		fixture.f_name = "";
		fixture.LastName = new JTextField();
		fixture.labelCard = new JLabel();
		fixture.labelNumber = new JLabel();
		fixture.panelFirstName = new JPanel();
		fixture.Card = new JTextField();
		fixture.FirstName = new JTextField();
		fixture.labelFirstName = new JLabel();
		fixture.c_number = "";
		fixture.pw = "";
		fixture.panelNumber = new JPanel();
		fixture.email = "";
		fixture.Email = new JTextField();
		fixture.panelLastName = new JPanel();
		fixture.panelNewID = new JPanel();
		fixture.panelNewPW = new JPanel();
		fixture.NewPW = new JPasswordField();
		fixture.panelCard = new JPanel();
		fixture.p_number = "";
		fixture.FEnroll = new JFrame();
		fixture.labelLastName = new JLabel();
		fixture.Number = new JTextField();
		fixture.panelEmail = new JPanel();
		fixture.NewID = new JTextField();
		fixture.l_name = "";
		fixture.access = new JButton();
		fixture.id = "";
		fixture.labelEmail = new JLabel();
		fixture.panelButton = new JPanel();
		ActionEvent e = new ActionEvent(new Object(), 1, "");

		fixture.actionPerformed(e);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfo(Server_sqlHelper.java:41)
		//       at LoginPage.setFlightData(LoginPage.java:74)
		//       at LoginPage.<init>(LoginPage.java:120)
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(buttonEnrollTest.class);
	}
}