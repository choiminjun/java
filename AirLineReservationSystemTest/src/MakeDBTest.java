import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>MakeDBTest</code> contains tests for the class <code>{@link MakeDB}</code>.
 *
 * @generatedBy CodePro at 14. 6. 10 오후 4:27
 * @author MINJUN
 * @version $Revision: 1.0 $
 */
public class MakeDBTest {
	/**
	 * Run the MakeDB(String) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testMakeDB_1()
		throws Exception {
		String fileName_in = "";

		MakeDB result = new MakeDB(fileName_in);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the void connectionSQL() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testConnectionSQL_1()
		throws Exception {
		MakeDB fixture = new MakeDB("");
		fixture.setFileName("");

		fixture.connectionSQL();

		// add additional test code here
	}

	/**
	 * Run the void connectionSQL() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testConnectionSQL_2()
		throws Exception {
		MakeDB fixture = new MakeDB("");
		fixture.setFileName("");

		fixture.connectionSQL();

		// add additional test code here
	}

	/**
	 * Run the void connectionSQL() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testConnectionSQL_3()
		throws Exception {
		MakeDB fixture = new MakeDB("");
		fixture.setFileName("");

		fixture.connectionSQL();

		// add additional test code here
	}

	/**
	 * Run the void enrollment(String,String,String,String,String,String,String,String,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testEnrollment_1()
		throws Exception {
		MakeDB fixture = new MakeDB("");
		fixture.setFileName("");
		String a = "";
		String b = "";
		String c = "";
		String d = "";
		String e = "";
		String f = "";
		String g = "";
		String h = "";
		String i = "";

		fixture.enrollment(a, b, c, d, e, f, g, h, i);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at MakeDB.enrollment(MakeDB.java:87)
	}

	/**
	 * Run the void enrollment(String,String,String,String,String,String,String,String,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testEnrollment_2()
		throws Exception {
		MakeDB fixture = new MakeDB("");
		fixture.setFileName("");
		String a = "";
		String b = "";
		String c = "";
		String d = "";
		String e = "";
		String f = "";
		String g = "";
		String h = "";
		String i = "";

		fixture.enrollment(a, b, c, d, e, f, g, h, i);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at MakeDB.enrollment(MakeDB.java:87)
	}

	/**
	 * Run the void enrollment(String,String,String,String,String,String,String,String,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testEnrollment_3()
		throws Exception {
		MakeDB fixture = new MakeDB("");
		fixture.setFileName("");
		String a = "";
		String b = "";
		String c = "";
		String d = "";
		String e = "";
		String f = "";
		String g = "";
		String h = "";
		String i = "";

		fixture.enrollment(a, b, c, d, e, f, g, h, i);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at MakeDB.enrollment(MakeDB.java:87)
	}

	/**
	 * Run the void enrollment(String,String,String,String,String,String,String,String,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testEnrollment_4()
		throws Exception {
		MakeDB fixture = new MakeDB("");
		fixture.setFileName("");
		String a = "";
		String b = "";
		String c = "";
		String d = "";
		String e = "";
		String f = "";
		String g = "";
		String h = "";
		String i = "";

		fixture.enrollment(a, b, c, d, e, f, g, h, i);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at MakeDB.enrollment(MakeDB.java:87)
	}

	/**
	 * Run the void enrollment(String,String,String,String,String,String,String,String,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testEnrollment_5()
		throws Exception {
		MakeDB fixture = new MakeDB("");
		fixture.setFileName("");
		String a = "";
		String b = "";
		String c = "";
		String d = "";
		String e = "";
		String f = "";
		String g = "";
		String h = "";
		String i = "";

		fixture.enrollment(a, b, c, d, e, f, g, h, i);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at MakeDB.enrollment(MakeDB.java:87)
	}

	/**
	 * Run the void enrollment(String,String,String,String,String,String,String,String,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testEnrollment_6()
		throws Exception {
		MakeDB fixture = new MakeDB("");
		fixture.setFileName("");
		String a = "";
		String b = "";
		String c = "";
		String d = "";
		String e = "";
		String f = "";
		String g = "";
		String h = "";
		String i = "";

		fixture.enrollment(a, b, c, d, e, f, g, h, i);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at MakeDB.enrollment(MakeDB.java:87)
	}

	/**
	 * Run the void enrollment(String,String,String,String,String,String,String,String,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testEnrollment_7()
		throws Exception {
		MakeDB fixture = new MakeDB("");
		fixture.setFileName("");
		String a = "";
		String b = "";
		String c = "";
		String d = "";
		String e = "";
		String f = "";
		String g = "";
		String h = "";
		String i = "";

		fixture.enrollment(a, b, c, d, e, f, g, h, i);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at MakeDB.enrollment(MakeDB.java:87)
	}

	/**
	 * Run the void enrollment(String,String,String,String,String,String,String,String,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testEnrollment_8()
		throws Exception {
		MakeDB fixture = new MakeDB("");
		fixture.setFileName("");
		String a = "";
		String b = "";
		String c = "";
		String d = "";
		String e = "";
		String f = "";
		String g = "";
		String h = "";
		String i = "";

		fixture.enrollment(a, b, c, d, e, f, g, h, i);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at MakeDB.enrollment(MakeDB.java:87)
	}

	/**
	 * Run the void enrollment(String,String,String,String,String,String,String,String,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testEnrollment_9()
		throws Exception {
		MakeDB fixture = new MakeDB("");
		fixture.setFileName("");
		String a = "";
		String b = "";
		String c = "";
		String d = "";
		String e = "";
		String f = "";
		String g = "";
		String h = "";
		String i = "";

		fixture.enrollment(a, b, c, d, e, f, g, h, i);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at MakeDB.enrollment(MakeDB.java:87)
	}

	/**
	 * Run the void enrollment(String,String,String,String,String,String,String,String,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testEnrollment_10()
		throws Exception {
		MakeDB fixture = new MakeDB("");
		fixture.setFileName("");
		String a = "";
		String b = "";
		String c = "";
		String d = "";
		String e = "";
		String f = "";
		String g = "";
		String h = "";
		String i = "";

		fixture.enrollment(a, b, c, d, e, f, g, h, i);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at MakeDB.enrollment(MakeDB.java:87)
	}

	/**
	 * Run the void enrollment(String,String,String,String,String,String,String,String,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testEnrollment_11()
		throws Exception {
		MakeDB fixture = new MakeDB("");
		fixture.setFileName("");
		String a = "";
		String b = "";
		String c = "";
		String d = "";
		String e = "";
		String f = "";
		String g = "";
		String h = "";
		String i = "";

		fixture.enrollment(a, b, c, d, e, f, g, h, i);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at MakeDB.enrollment(MakeDB.java:87)
	}

	/**
	 * Run the void enrollment(String,String,String,String,String,String,String,String,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testEnrollment_12()
		throws Exception {
		MakeDB fixture = new MakeDB("");
		fixture.setFileName("");
		String a = "";
		String b = "";
		String c = "";
		String d = "";
		String e = "";
		String f = "";
		String g = "";
		String h = "";
		String i = "";

		fixture.enrollment(a, b, c, d, e, f, g, h, i);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at MakeDB.enrollment(MakeDB.java:87)
	}

	/**
	 * Run the void enrollment(String,String,String,String,String,String,String,String,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testEnrollment_13()
		throws Exception {
		MakeDB fixture = new MakeDB("");
		fixture.setFileName("");
		String a = "";
		String b = "";
		String c = "";
		String d = "";
		String e = "";
		String f = "";
		String g = "";
		String h = "";
		String i = "";

		fixture.enrollment(a, b, c, d, e, f, g, h, i);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at MakeDB.enrollment(MakeDB.java:87)
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testMain_1()
		throws Exception {
		String[] args = new String[] {};

		MakeDB.main(args);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Exit while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkExit(CodeProJUnitSecurityManager.java:57)
		//       at java.lang.Runtime.exit(Runtime.java:88)
		//       at java.lang.System.exit(System.java:920)
		//       at MakeDB.readAndWriteFile(MakeDB.java:42)
		//       at MakeDB.main(MakeDB.java:116)
	}

	/**
	 * Run the void readAndWriteFile() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testReadAndWriteFile_1()
		throws Exception {
		MakeDB fixture = new MakeDB("");
		fixture.setFileName("");

		fixture.readAndWriteFile();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Exit while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkExit(CodeProJUnitSecurityManager.java:57)
		//       at java.lang.Runtime.exit(Runtime.java:88)
		//       at java.lang.System.exit(System.java:920)
		//       at MakeDB.readAndWriteFile(MakeDB.java:42)
	}

	/**
	 * Run the void readAndWriteFile() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testReadAndWriteFile_2()
		throws Exception {
		MakeDB fixture = new MakeDB("");
		fixture.setFileName("");

		fixture.readAndWriteFile();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Exit while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkExit(CodeProJUnitSecurityManager.java:57)
		//       at java.lang.Runtime.exit(Runtime.java:88)
		//       at java.lang.System.exit(System.java:920)
		//       at MakeDB.readAndWriteFile(MakeDB.java:42)
	}

	/**
	 * Run the void readAndWriteFile() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testReadAndWriteFile_3()
		throws Exception {
		MakeDB fixture = new MakeDB("");
		fixture.setFileName("");

		fixture.readAndWriteFile();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Exit while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkExit(CodeProJUnitSecurityManager.java:57)
		//       at java.lang.Runtime.exit(Runtime.java:88)
		//       at java.lang.System.exit(System.java:920)
		//       at MakeDB.readAndWriteFile(MakeDB.java:42)
	}

	/**
	 * Run the void readAndWriteFile() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testReadAndWriteFile_4()
		throws Exception {
		MakeDB fixture = new MakeDB("");
		fixture.setFileName("");

		fixture.readAndWriteFile();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Exit while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkExit(CodeProJUnitSecurityManager.java:57)
		//       at java.lang.Runtime.exit(Runtime.java:88)
		//       at java.lang.System.exit(System.java:920)
		//       at MakeDB.readAndWriteFile(MakeDB.java:42)
	}

	/**
	 * Run the void readAndWriteFile() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testReadAndWriteFile_5()
		throws Exception {
		MakeDB fixture = new MakeDB("");
		fixture.setFileName("");

		fixture.readAndWriteFile();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Exit while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkExit(CodeProJUnitSecurityManager.java:57)
		//       at java.lang.Runtime.exit(Runtime.java:88)
		//       at java.lang.System.exit(System.java:920)
		//       at MakeDB.readAndWriteFile(MakeDB.java:42)
	}

	/**
	 * Run the void readAndWriteFile() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testReadAndWriteFile_6()
		throws Exception {
		MakeDB fixture = new MakeDB("");
		fixture.setFileName("");

		fixture.readAndWriteFile();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Exit while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkExit(CodeProJUnitSecurityManager.java:57)
		//       at java.lang.Runtime.exit(Runtime.java:88)
		//       at java.lang.System.exit(System.java:920)
		//       at MakeDB.readAndWriteFile(MakeDB.java:42)
	}

	/**
	 * Run the void setFileName(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testSetFileName_1()
		throws Exception {
		MakeDB fixture = new MakeDB("");
		fixture.setFileName("");
		String fileName_in = "";

		fixture.setFileName(fileName_in);

		// add additional test code here
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(MakeDBTest.class);
	}
}