import java.awt.event.ActionEvent;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>button3Test</code> contains tests for the class <code>{@link button3}</code>.
 *
 * @generatedBy CodePro at 14. 6. 10 오후 4:27
 * @author MINJUN
 * @version $Revision: 1.0 $
 */
public class button3Test {
	/**
	 * Run the button3(DomesticFlight,LoginPage) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testButton3_1()
		throws Exception {
		DomesticFlight type = new DomesticFlight(new LoginPage());
		LoginPage type1 = new LoginPage();

		button3 result = new button3(type, type1);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfo(Server_sqlHelper.java:41)
		//       at LoginPage.setFlightData(LoginPage.java:74)
		//       at LoginPage.<init>(LoginPage.java:120)
		assertNotNull(result);
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(button3Test.class);
	}
}