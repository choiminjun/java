import java.awt.Frame;
import java.awt.Window;
import java.awt.event.WindowEvent;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>ExitListenerTest</code> contains tests for the class <code>{@link ExitListener}</code>.
 *
 * @generatedBy CodePro at 14. 6. 10 오후 4:27
 * @author MINJUN
 * @version $Revision: 1.0 $
 */
public class ExitListenerTest {
	/**
	 * Run the void windowClosing(WindowEvent) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testWindowClosing_1()
		throws Exception {
		ExitListener fixture = new ExitListener();
		WindowEvent event = new WindowEvent(new Window(new Frame()), 1);

		fixture.windowClosing(event);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.SecurityException: Exit while generating test cases
		//       at com.instantiations.assist.eclipse.junit.CodeProJUnitSecurityManager.checkExit(CodeProJUnitSecurityManager.java:57)
		//       at java.lang.Runtime.exit(Runtime.java:88)
		//       at java.lang.System.exit(System.java:920)
		//       at ExitListener.windowClosing(ExitListener.java:8)
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(ExitListenerTest.class);
	}
}