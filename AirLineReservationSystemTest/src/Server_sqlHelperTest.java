import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>Server_sqlHelperTest</code> contains tests for the class <code>{@link Server_sqlHelper}</code>.
 *
 * @generatedBy CodePro at 14. 6. 10 오후 4:27
 * @author MINJUN
 * @version $Revision: 1.0 $
 */
public class Server_sqlHelperTest {
	/**
	 * Run the Server_sqlHelper() constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testServer_sqlHelper_1()
		throws Exception {

		Server_sqlHelper result = new Server_sqlHelper();

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the boolean chkID(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testChkID_1()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();
		String ID = "";

		boolean result = fixture.chkID(ID);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.chkID(Server_sqlHelper.java:250)
		assertTrue(result);
	}

	/**
	 * Run the boolean chkID(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testChkID_2()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();
		String ID = "";

		boolean result = fixture.chkID(ID);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.chkID(Server_sqlHelper.java:250)
		assertTrue(result);
	}

	/**
	 * Run the boolean chkID(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testChkID_3()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();
		String ID = "";

		boolean result = fixture.chkID(ID);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.chkID(Server_sqlHelper.java:250)
		assertTrue(result);
	}

	/**
	 * Run the boolean chkID(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testChkID_4()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();
		String ID = "";

		boolean result = fixture.chkID(ID);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.chkID(Server_sqlHelper.java:250)
		assertTrue(result);
	}

	/**
	 * Run the boolean chkID(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testChkID_5()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();
		String ID = "";

		boolean result = fixture.chkID(ID);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.chkID(Server_sqlHelper.java:250)
		assertTrue(result);
	}

	/**
	 * Run the boolean chkID(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testChkID_6()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();
		String ID = "";

		boolean result = fixture.chkID(ID);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.chkID(Server_sqlHelper.java:250)
		assertTrue(result);
	}

	/**
	 * Run the boolean chkID(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testChkID_7()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();
		String ID = "";

		boolean result = fixture.chkID(ID);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.chkID(Server_sqlHelper.java:250)
		assertTrue(result);
	}

	/**
	 * Run the boolean chkID(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testChkID_8()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();
		String ID = "";

		boolean result = fixture.chkID(ID);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.chkID(Server_sqlHelper.java:250)
		assertTrue(result);
	}

	/**
	 * Run the boolean chkID(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testChkID_9()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();
		String ID = "";

		boolean result = fixture.chkID(ID);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.chkID(Server_sqlHelper.java:250)
		assertTrue(result);
	}

	/**
	 * Run the boolean chkID(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testChkID_10()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();
		String ID = "";

		boolean result = fixture.chkID(ID);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.chkID(Server_sqlHelper.java:250)
		assertTrue(result);
	}

	/**
	 * Run the boolean chkID(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testChkID_11()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();
		String ID = "";

		boolean result = fixture.chkID(ID);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.chkID(Server_sqlHelper.java:250)
		assertTrue(result);
	}

	/**
	 * Run the boolean chkID(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testChkID_12()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();
		String ID = "";

		boolean result = fixture.chkID(ID);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.chkID(Server_sqlHelper.java:250)
		assertTrue(result);
	}

	/**
	 * Run the void connectionSQL() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testConnectionSQL_1()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		fixture.connectionSQL();

		// add additional test code here
	}

	/**
	 * Run the void connectionSQL() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testConnectionSQL_2()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		fixture.connectionSQL();

		// add additional test code here
	}

	/**
	 * Run the void connectionSQL() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testConnectionSQL_3()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		fixture.connectionSQL();

		// add additional test code here
	}

	/**
	 * Run the void enrollment(String,String,String,String,String,String,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testEnrollment_1()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();
		String id = "";
		String pw = "";
		String f_name = "";
		String l_name = "";
		String email = "";
		String p_number = "";
		String c_number = "";

		fixture.enrollment(id, pw, f_name, l_name, email, p_number, c_number);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.enrollment(Server_sqlHelper.java:296)
	}

	/**
	 * Run the void enrollment(String,String,String,String,String,String,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testEnrollment_2()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();
		String id = "";
		String pw = "";
		String f_name = "";
		String l_name = "";
		String email = "";
		String p_number = "";
		String c_number = "";

		fixture.enrollment(id, pw, f_name, l_name, email, p_number, c_number);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.enrollment(Server_sqlHelper.java:296)
	}

	/**
	 * Run the void enrollment(String,String,String,String,String,String,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testEnrollment_3()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();
		String id = "";
		String pw = "";
		String f_name = "";
		String l_name = "";
		String email = "";
		String p_number = "";
		String c_number = "";

		fixture.enrollment(id, pw, f_name, l_name, email, p_number, c_number);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.enrollment(Server_sqlHelper.java:296)
	}

	/**
	 * Run the void enrollment(String,String,String,String,String,String,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testEnrollment_4()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();
		String id = "";
		String pw = "";
		String f_name = "";
		String l_name = "";
		String email = "";
		String p_number = "";
		String c_number = "";

		fixture.enrollment(id, pw, f_name, l_name, email, p_number, c_number);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.enrollment(Server_sqlHelper.java:296)
	}

	/**
	 * Run the void enrollment(String,String,String,String,String,String,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testEnrollment_5()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();
		String id = "";
		String pw = "";
		String f_name = "";
		String l_name = "";
		String email = "";
		String p_number = "";
		String c_number = "";

		fixture.enrollment(id, pw, f_name, l_name, email, p_number, c_number);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.enrollment(Server_sqlHelper.java:296)
	}

	/**
	 * Run the void enrollment(String,String,String,String,String,String,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testEnrollment_6()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();
		String id = "";
		String pw = "";
		String f_name = "";
		String l_name = "";
		String email = "";
		String p_number = "";
		String c_number = "";

		fixture.enrollment(id, pw, f_name, l_name, email, p_number, c_number);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.enrollment(Server_sqlHelper.java:296)
	}

	/**
	 * Run the void enrollment(String,String,String,String,String,String,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testEnrollment_7()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();
		String id = "";
		String pw = "";
		String f_name = "";
		String l_name = "";
		String email = "";
		String p_number = "";
		String c_number = "";

		fixture.enrollment(id, pw, f_name, l_name, email, p_number, c_number);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.enrollment(Server_sqlHelper.java:296)
	}

	/**
	 * Run the void enrollment(String,String,String,String,String,String,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testEnrollment_8()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();
		String id = "";
		String pw = "";
		String f_name = "";
		String l_name = "";
		String email = "";
		String p_number = "";
		String c_number = "";

		fixture.enrollment(id, pw, f_name, l_name, email, p_number, c_number);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.enrollment(Server_sqlHelper.java:296)
	}

	/**
	 * Run the void enrollment(String,String,String,String,String,String,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testEnrollment_9()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();
		String id = "";
		String pw = "";
		String f_name = "";
		String l_name = "";
		String email = "";
		String p_number = "";
		String c_number = "";

		fixture.enrollment(id, pw, f_name, l_name, email, p_number, c_number);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.enrollment(Server_sqlHelper.java:296)
	}

	/**
	 * Run the void enrollment(String,String,String,String,String,String,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testEnrollment_10()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();
		String id = "";
		String pw = "";
		String f_name = "";
		String l_name = "";
		String email = "";
		String p_number = "";
		String c_number = "";

		fixture.enrollment(id, pw, f_name, l_name, email, p_number, c_number);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.enrollment(Server_sqlHelper.java:296)
	}

	/**
	 * Run the void enrollment(String,String,String,String,String,String,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testEnrollment_11()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();
		String id = "";
		String pw = "";
		String f_name = "";
		String l_name = "";
		String email = "";
		String p_number = "";
		String c_number = "";

		fixture.enrollment(id, pw, f_name, l_name, email, p_number, c_number);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.enrollment(Server_sqlHelper.java:296)
	}

	/**
	 * Run the void enrollment(String,String,String,String,String,String,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testEnrollment_12()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();
		String id = "";
		String pw = "";
		String f_name = "";
		String l_name = "";
		String email = "";
		String p_number = "";
		String c_number = "";

		fixture.enrollment(id, pw, f_name, l_name, email, p_number, c_number);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.enrollment(Server_sqlHelper.java:296)
	}

	/**
	 * Run the String[][] getFlightInfo() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfo_1()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfo();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfo(Server_sqlHelper.java:41)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfo() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfo_2()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfo();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfo(Server_sqlHelper.java:41)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfo() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfo_3()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfo();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfo(Server_sqlHelper.java:41)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfo() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfo_4()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfo();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfo(Server_sqlHelper.java:41)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfo() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfo_5()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfo();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfo(Server_sqlHelper.java:41)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfo() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfo_6()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfo();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfo(Server_sqlHelper.java:41)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfo() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfo_7()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfo();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfo(Server_sqlHelper.java:41)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfo() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfo_8()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfo();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfo(Server_sqlHelper.java:41)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfo() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfo_9()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfo();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfo(Server_sqlHelper.java:41)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfo() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfo_10()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfo();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfo(Server_sqlHelper.java:41)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfo() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfo_11()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfo();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfo(Server_sqlHelper.java:41)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfo() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfo_12()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfo();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfo(Server_sqlHelper.java:41)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfo() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfo_13()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfo();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfo(Server_sqlHelper.java:41)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfo() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfo_14()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfo();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfo(Server_sqlHelper.java:41)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfo() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfo_15()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfo();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfo(Server_sqlHelper.java:41)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfo() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfo_16()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfo();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfo(Server_sqlHelper.java:41)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfoDome_bus() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfoDome_bus_1()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfoDome_bus();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfoDome_bus(Server_sqlHelper.java:81)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfoDome_bus() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfoDome_bus_2()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfoDome_bus();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfoDome_bus(Server_sqlHelper.java:81)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfoDome_bus() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfoDome_bus_3()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfoDome_bus();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfoDome_bus(Server_sqlHelper.java:81)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfoDome_bus() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfoDome_bus_4()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfoDome_bus();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfoDome_bus(Server_sqlHelper.java:81)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfoDome_bus() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfoDome_bus_5()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfoDome_bus();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfoDome_bus(Server_sqlHelper.java:81)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfoDome_bus() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfoDome_bus_6()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfoDome_bus();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfoDome_bus(Server_sqlHelper.java:81)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfoDome_bus() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfoDome_bus_7()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfoDome_bus();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfoDome_bus(Server_sqlHelper.java:81)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfoDome_bus() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfoDome_bus_8()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfoDome_bus();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfoDome_bus(Server_sqlHelper.java:81)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfoDome_bus() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfoDome_bus_9()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfoDome_bus();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfoDome_bus(Server_sqlHelper.java:81)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfoDome_bus() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfoDome_bus_10()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfoDome_bus();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfoDome_bus(Server_sqlHelper.java:81)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfoDome_bus() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfoDome_bus_11()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfoDome_bus();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfoDome_bus(Server_sqlHelper.java:81)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfoDome_bus() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfoDome_bus_12()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfoDome_bus();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfoDome_bus(Server_sqlHelper.java:81)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfoDome_bus() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfoDome_bus_13()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfoDome_bus();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfoDome_bus(Server_sqlHelper.java:81)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfoDome_bus() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfoDome_bus_14()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfoDome_bus();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfoDome_bus(Server_sqlHelper.java:81)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfoDome_bus() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfoDome_bus_15()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfoDome_bus();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfoDome_bus(Server_sqlHelper.java:81)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfoDome_bus() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfoDome_bus_16()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfoDome_bus();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfoDome_bus(Server_sqlHelper.java:81)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfoInter() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfoInter_1()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfoInter();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfoInter(Server_sqlHelper.java:121)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfoInter() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfoInter_2()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfoInter();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfoInter(Server_sqlHelper.java:121)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfoInter() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfoInter_3()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfoInter();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfoInter(Server_sqlHelper.java:121)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfoInter() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfoInter_4()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfoInter();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfoInter(Server_sqlHelper.java:121)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfoInter() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfoInter_5()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfoInter();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfoInter(Server_sqlHelper.java:121)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfoInter() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfoInter_6()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfoInter();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfoInter(Server_sqlHelper.java:121)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfoInter() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfoInter_7()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfoInter();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfoInter(Server_sqlHelper.java:121)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfoInter() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfoInter_8()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfoInter();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfoInter(Server_sqlHelper.java:121)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfoInter() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfoInter_9()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfoInter();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfoInter(Server_sqlHelper.java:121)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfoInter() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfoInter_10()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfoInter();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfoInter(Server_sqlHelper.java:121)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfoInter() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfoInter_11()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfoInter();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfoInter(Server_sqlHelper.java:121)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfoInter() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfoInter_12()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfoInter();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfoInter(Server_sqlHelper.java:121)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfoInter() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfoInter_13()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfoInter();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfoInter(Server_sqlHelper.java:121)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfoInter() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfoInter_14()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfoInter();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfoInter(Server_sqlHelper.java:121)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfoInter() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfoInter_15()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfoInter();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfoInter(Server_sqlHelper.java:121)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfoInter() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfoInter_16()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfoInter();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfoInter(Server_sqlHelper.java:121)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfoInterBus() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfoInterBus_1()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfoInterBus();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfoInterBus(Server_sqlHelper.java:161)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfoInterBus() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfoInterBus_2()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfoInterBus();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfoInterBus(Server_sqlHelper.java:161)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfoInterBus() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfoInterBus_3()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfoInterBus();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfoInterBus(Server_sqlHelper.java:161)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfoInterBus() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfoInterBus_4()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfoInterBus();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfoInterBus(Server_sqlHelper.java:161)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfoInterBus() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfoInterBus_5()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfoInterBus();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfoInterBus(Server_sqlHelper.java:161)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfoInterBus() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfoInterBus_6()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfoInterBus();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfoInterBus(Server_sqlHelper.java:161)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfoInterBus() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfoInterBus_7()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfoInterBus();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfoInterBus(Server_sqlHelper.java:161)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfoInterBus() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfoInterBus_8()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfoInterBus();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfoInterBus(Server_sqlHelper.java:161)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfoInterBus() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfoInterBus_9()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfoInterBus();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfoInterBus(Server_sqlHelper.java:161)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfoInterBus() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfoInterBus_10()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfoInterBus();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfoInterBus(Server_sqlHelper.java:161)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfoInterBus() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfoInterBus_11()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfoInterBus();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfoInterBus(Server_sqlHelper.java:161)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfoInterBus() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfoInterBus_12()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfoInterBus();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfoInterBus(Server_sqlHelper.java:161)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfoInterBus() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfoInterBus_13()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfoInterBus();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfoInterBus(Server_sqlHelper.java:161)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfoInterBus() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfoInterBus_14()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfoInterBus();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfoInterBus(Server_sqlHelper.java:161)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfoInterBus() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfoInterBus_15()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfoInterBus();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfoInterBus(Server_sqlHelper.java:161)
		assertNotNull(result);
	}

	/**
	 * Run the String[][] getFlightInfoInterBus() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testGetFlightInfoInterBus_16()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();

		String[][] result = fixture.getFlightInfoInterBus();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfoInterBus(Server_sqlHelper.java:161)
		assertNotNull(result);
	}

	/**
	 * Run the boolean login(String,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testLogin_1()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();
		String ID = "";
		String password = "";

		boolean result = fixture.login(ID, password);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.login(Server_sqlHelper.java:203)
		assertTrue(result);
	}

	/**
	 * Run the boolean login(String,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testLogin_2()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();
		String ID = "";
		String password = "";

		boolean result = fixture.login(ID, password);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.login(Server_sqlHelper.java:203)
		assertTrue(result);
	}

	/**
	 * Run the boolean login(String,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testLogin_3()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();
		String ID = "";
		String password = "";

		boolean result = fixture.login(ID, password);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.login(Server_sqlHelper.java:203)
		assertTrue(result);
	}

	/**
	 * Run the boolean login(String,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testLogin_4()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();
		String ID = "";
		String password = "";

		boolean result = fixture.login(ID, password);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.login(Server_sqlHelper.java:203)
		assertTrue(result);
	}

	/**
	 * Run the boolean login(String,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testLogin_5()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();
		String ID = "";
		String password = "";

		boolean result = fixture.login(ID, password);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.login(Server_sqlHelper.java:203)
		assertTrue(result);
	}

	/**
	 * Run the boolean login(String,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testLogin_6()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();
		String ID = "";
		String password = "";

		boolean result = fixture.login(ID, password);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.login(Server_sqlHelper.java:203)
		assertTrue(result);
	}

	/**
	 * Run the boolean login(String,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testLogin_7()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();
		String ID = "";
		String password = "";

		boolean result = fixture.login(ID, password);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.login(Server_sqlHelper.java:203)
		assertTrue(result);
	}

	/**
	 * Run the boolean login(String,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testLogin_8()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();
		String ID = "";
		String password = "";

		boolean result = fixture.login(ID, password);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.login(Server_sqlHelper.java:203)
		assertTrue(result);
	}

	/**
	 * Run the boolean login(String,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testLogin_9()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();
		String ID = "";
		String password = "";

		boolean result = fixture.login(ID, password);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.login(Server_sqlHelper.java:203)
		assertTrue(result);
	}

	/**
	 * Run the boolean login(String,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testLogin_10()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();
		String ID = "";
		String password = "";

		boolean result = fixture.login(ID, password);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.login(Server_sqlHelper.java:203)
		assertTrue(result);
	}

	/**
	 * Run the boolean login(String,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testLogin_11()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();
		String ID = "";
		String password = "";

		boolean result = fixture.login(ID, password);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.login(Server_sqlHelper.java:203)
		assertTrue(result);
	}

	/**
	 * Run the boolean login(String,String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testLogin_12()
		throws Exception {
		Server_sqlHelper fixture = new Server_sqlHelper();
		String ID = "";
		String password = "";

		boolean result = fixture.login(ID, password);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.login(Server_sqlHelper.java:203)
		assertTrue(result);
	}

	/**
	 * Run the void main(String[]) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testMain_1()
		throws Exception {
		String[] args = new String[] {};

		Server_sqlHelper.main(args);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfo(Server_sqlHelper.java:41)
		//       at Server_sqlHelper.main(Server_sqlHelper.java:325)
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(Server_sqlHelperTest.class);
	}
}