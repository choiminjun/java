import java.awt.Component;
import java.awt.event.MouseEvent;
import javax.swing.Box;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>mouse2Test</code> contains tests for the class <code>{@link mouse2}</code>.
 *
 * @generatedBy CodePro at 14. 6. 10 오후 4:27
 * @author MINJUN
 * @version $Revision: 1.0 $
 */
public class mouse2Test {
	/**
	 * Run the mouse2(LoginPage,boolean) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testMouse2_1()
		throws Exception {
		LoginPage type = new LoginPage();
		boolean bCheck = true;

		mouse2 result = new mouse2(type, bCheck);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfo(Server_sqlHelper.java:41)
		//       at LoginPage.setFlightData(LoginPage.java:74)
		//       at LoginPage.<init>(LoginPage.java:120)
		assertNotNull(result);
	}

	/**
	 * Run the void mouseClicked(MouseEvent) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testMouseClicked_1()
		throws Exception {
		mouse2 fixture = new mouse2(new LoginPage(), false);
		MouseEvent e = new MouseEvent(Box.createGlue(), 1, 1L, 1, 1, 1, 1, true);

		fixture.mouseClicked(e);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfo(Server_sqlHelper.java:41)
		//       at LoginPage.setFlightData(LoginPage.java:74)
		//       at LoginPage.<init>(LoginPage.java:120)
	}

	/**
	 * Run the void mouseClicked(MouseEvent) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testMouseClicked_2()
		throws Exception {
		mouse2 fixture = new mouse2(new LoginPage(), true);
		MouseEvent e = new MouseEvent(Box.createGlue(), 1, 1L, 1, 1, 1, 1, true);

		fixture.mouseClicked(e);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfo(Server_sqlHelper.java:41)
		//       at LoginPage.setFlightData(LoginPage.java:74)
		//       at LoginPage.<init>(LoginPage.java:120)
	}

	/**
	 * Run the void mouseClicked(MouseEvent) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testMouseClicked_3()
		throws Exception {
		mouse2 fixture = new mouse2(new LoginPage(), false);
		MouseEvent e = new MouseEvent(Box.createGlue(), 1, 1L, 1, 1, 1, 1, true);

		fixture.mouseClicked(e);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfo(Server_sqlHelper.java:41)
		//       at LoginPage.setFlightData(LoginPage.java:74)
		//       at LoginPage.<init>(LoginPage.java:120)
	}

	/**
	 * Run the void mouseClicked(MouseEvent) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testMouseClicked_4()
		throws Exception {
		mouse2 fixture = new mouse2(new LoginPage(), true);
		MouseEvent e = new MouseEvent(Box.createGlue(), 1, 1L, 1, 1, 1, 1, true);

		fixture.mouseClicked(e);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfo(Server_sqlHelper.java:41)
		//       at LoginPage.setFlightData(LoginPage.java:74)
		//       at LoginPage.<init>(LoginPage.java:120)
	}

	/**
	 * Run the void mouseEntered(MouseEvent) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testMouseEntered_1()
		throws Exception {
		mouse2 fixture = new mouse2(new LoginPage(), true);
		MouseEvent e = new MouseEvent(Box.createGlue(), 1, 1L, 1, 1, 1, 1, true);

		fixture.mouseEntered(e);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfo(Server_sqlHelper.java:41)
		//       at LoginPage.setFlightData(LoginPage.java:74)
		//       at LoginPage.<init>(LoginPage.java:120)
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(mouse2Test.class);
	}
}