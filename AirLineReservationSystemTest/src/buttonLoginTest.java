import java.awt.event.ActionEvent;
import javax.swing.JFrame;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>buttonLoginTest</code> contains tests for the class <code>{@link buttonLogin}</code>.
 *
 * @generatedBy CodePro at 14. 6. 10 오후 4:27
 * @author MINJUN
 * @version $Revision: 1.0 $
 */
public class buttonLoginTest {
	/**
	 * Run the buttonLogin(LoginPage) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testButtonLogin_1()
		throws Exception {
		LoginPage type = new LoginPage();

		buttonLogin result = new buttonLogin(type);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfo(Server_sqlHelper.java:41)
		//       at LoginPage.setFlightData(LoginPage.java:74)
		//       at LoginPage.<init>(LoginPage.java:120)
		assertNotNull(result);
	}

	/**
	 * Run the void actionPerformed(ActionEvent) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testActionPerformed_1()
		throws Exception {
		buttonLogin fixture = new buttonLogin(new LoginPage());
		fixture.id_input = "";
		fixture.isLogin = true;
		fixture.pw_input = "";
		fixture.f = new JFrame();
		ActionEvent e = new ActionEvent(new Object(), 1, "");

		fixture.actionPerformed(e);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfo(Server_sqlHelper.java:41)
		//       at LoginPage.setFlightData(LoginPage.java:74)
		//       at LoginPage.<init>(LoginPage.java:120)
	}

	/**
	 * Run the void actionPerformed(ActionEvent) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testActionPerformed_2()
		throws Exception {
		buttonLogin fixture = new buttonLogin(new LoginPage());
		fixture.id_input = "";
		fixture.isLogin = true;
		fixture.pw_input = "";
		fixture.f = new JFrame();
		ActionEvent e = new ActionEvent(new Object(), 1, "");

		fixture.actionPerformed(e);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfo(Server_sqlHelper.java:41)
		//       at LoginPage.setFlightData(LoginPage.java:74)
		//       at LoginPage.<init>(LoginPage.java:120)
	}

	/**
	 * Run the void actionPerformed(ActionEvent) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Test
	public void testActionPerformed_3()
		throws Exception {
		buttonLogin fixture = new buttonLogin(new LoginPage());
		fixture.id_input = "";
		fixture.isLogin = true;
		fixture.pw_input = "";
		fixture.f = new JFrame();
		ActionEvent e = new ActionEvent(new Object(), 1, "");

		fixture.actionPerformed(e);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at Server_sqlHelper.getFlightInfo(Server_sqlHelper.java:41)
		//       at LoginPage.setFlightData(LoginPage.java:74)
		//       at LoginPage.<init>(LoginPage.java:120)
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:27
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(buttonLoginTest.class);
	}
}