/**
 * Program to Count(nonnegative numbers) Author : Choi Min Jun. Email Address:
 * choiminjun0720@gmail.com Programming Projects 5 - 2. Last Changed :
 * 04/04/2014.
 */
public class CounterDemo {
	public static void main(String[] args) {
		Counter s = new Counter(); // make class's object.
		s.countUp(); // countUp and countDown method implies display method
						// which is in Conter class's method.
		s.countDown();
		s.countUp();
		s.countDown();
		s.countDown();
		s.countUp();
		s.countUp();
		s.countUp();
	}
}
