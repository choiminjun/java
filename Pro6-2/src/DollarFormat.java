

public class DollarFormat {
	
	/**
	 * write method.
	 * @param amount
	 */
	public static void write(double amount) {
		if (amount >= 0) {
			System.out.print('$');
			writePositive(amount);
		} else {
			double positiveAmount = amount;
			System.out.print('$');
			System.out.print('-');
			System.out.print(positiveAmount);
		}
	}

	/**
	 * writePositive method
	 * @param amount
	 */
	private static void writePositive(double amount) {
		int allCents = (int) Math.floor(amount * 100);
		int dollars = allCents / 100;
		int cents = allCents % 100;

		System.out.print(dollars);
		System.out.print('.');

		if (cents < 10) {
			System.out.print('0');
		}
		System.out.print(cents);
	}

	public static void writeln(double amount) {
		write(amount);
		System.out.println();
	}
}
