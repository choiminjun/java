import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class LoginPage extends JFrame {
	Container c = getContentPane();
	JPanel PFlightTypes = new JPanel(null);
	JPanel PLogin = new JPanel(null);
	
	JPanel PFlightDetails = new JPanel(null);

	public boolean bCheck = true;

	JLabel LDomesticFlight = new JLabel("<html><B>Domestic Flights</B></html>");
	JLabel LInternationalFlight = new JLabel(
			"<html><B>International Flights</B></html>");

	JLabel LUserName, LPassword;

	JLabel LDomesticFlight1 = new JLabel(
			"<html><B>Domestic Flight Booking</B></html>");
	JLabel LInternationalFlight1 = new JLabel(
			"<html><B>International Flight Booking</B></html>");

	JTextField TFUserName;
	JPasswordField TPPassword;

	JButton BLogin;
	JButton Enroll;
	final Object[] col1 = { "Departure", "Arriaval", "DepartureTime",
			"ArivalTime", "Price", "Airline" };
	final Object[] col2 = { "Departure", "Arriaval", "DepartureTime",
			"ArivalTime", "Price", "Airline" };
	final Object[] col3 = { "Departure", "Arriaval", "DepartureTime",
			"ArivalTime", "Price", "Airline" };

	String[][] row1;
	String[][] row2;
	String[][] row3;
	String[][] row4;

	JTable TDomesticFlight;
	JTable TInternationalFlight;
	JTable TDomesticFlight1;
	JTable TInternationalFlight1;

	JScrollPane JSP1;
	JScrollPane JSP2;
	JScrollPane JSP3;
	JScrollPane JSP4;

	Icon img1;
	Icon img2;
	Icon img3;
	Icon img4;

	JLabel LEconomic;
	JLabel LBusiness;
	JLabel LEconomic1;
	JLabel LBusiness1;
	Server_sqlHelper test;
	
	public void setFlightData() {
		String[][] flightInfo = null;
		flightInfo = test.getFlightInfo();
		//db call

		row1 = new String[flightInfo.length][flightInfo[0].length];
		row2 = new String[flightInfo.length][flightInfo[0].length];
		row3 = new String[flightInfo.length][flightInfo[0].length];
		row4 = new String[flightInfo.length][flightInfo[0].length];

		for (int i = 0; i < flightInfo.length; i++) {
			for (int j = 0; j < flightInfo[i].length; j++) {
				row1[i][j] = flightInfo[i][j];
				row2[i][j] = flightInfo[i][j];
				row3[i][j] = flightInfo[i][j];
				row4[i][j] = flightInfo[i][j];
			}

		}
	}
	public static void main(String args[]) {
		LoginPage ars = new LoginPage();
		
	}

	public LoginPage() {
		test = new Server_sqlHelper();
		this.setFlightData();

		TDomesticFlight = new JTable(row1, col1);
		TInternationalFlight = new JTable(row2, col2);
		TDomesticFlight1 = new JTable(row3, col3);
		TInternationalFlight1 = new JTable(row4, col2);

		JSP1 = new JScrollPane(TDomesticFlight,
				ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		JSP2 = new JScrollPane(TInternationalFlight,
				ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		JSP3 = new JScrollPane(TDomesticFlight1,
				ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		JSP4 = new JScrollPane(TInternationalFlight1,
				ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);

		img1 = new ImageIcon("img/economic.jpg");
		img2 = new ImageIcon("img/business.jpg");
		img3 = new ImageIcon("img/economic1.jpg");
		img4 = new ImageIcon("img/business1.jpg");

		LEconomic = new JLabel("Economic", img1, SwingConstants.LEFT);
		LBusiness = new JLabel("Business", img2, SwingConstants.LEFT);
		LEconomic1 = new JLabel("Economic", img3, SwingConstants.LEFT);
		LBusiness1 = new JLabel("Business", img4, SwingConstants.LEFT);

		WindowUtilities.setNativeLookAndFeel();
		setPreferredSize(new Dimension(796, 572));

		PFlightTypes.setBackground(Color.white);
		
		PLogin.setBackground(Color.white);
		PFlightDetails.setBackground(Color.white);

		JSP1.setBounds(0, 340, 790, 200);
		JSP2.setBounds(0, 340, 790, 200);
		JSP3.setBounds(0, 340, 790, 200);
		JSP4.setBounds(0, 340, 790, 200);

		PFlightTypes.setBounds(0, 0, 500, 340);
		PLogin.setBounds(500, 0, 350, 340);
		PFlightDetails.setBounds(0, 340, 790, 200);

		LUserName = new JLabel("ID");
		LPassword = new JLabel("Password ");
		TFUserName = new JTextField(20);
		TPPassword = new JPasswordField(20);
		
		Enroll = new JButton("Enrollment");
		BLogin = new JButton("Login");

		LUserName.setBounds(40, 100, 100, 21);
		LPassword.setBounds(40, 140, 100, 21);
		TFUserName.setBounds(160, 100, 100, 21);
		TPPassword.setBounds(160, 140, 100, 21);
		Enroll.setBounds(60, 200, 100, 25);
		BLogin.setBounds(160, 200, 100, 25);

		LDomesticFlight1.setBounds(60, 60, 138, 20);
		LInternationalFlight1.setBounds(60, 100, 153, 20);

		PLogin.add(LUserName);
		PLogin.add(TFUserName);
		PLogin.add(LPassword);
		PLogin.add(TPPassword);
		PLogin.add(Enroll);
		PLogin.add(BLogin);

		PFlightDetails.add(JSP1);
		PFlightDetails.add(JSP2);
		PFlightDetails.add(JSP3);
		PFlightDetails.add(JSP4);

		JSP1.setVisible(true);
		JSP2.setVisible(false);
		JSP3.setVisible(false);
		JSP4.setVisible(false);

		LBusiness.setBounds(265, 170, 300, 125);
		LEconomic.setBounds(0, 170, 250, 125);
		LBusiness1.setBounds(280, 200, 135, 60);
		LEconomic1.setBounds(50, 200, 147, 60);

		PFlightTypes.add(LEconomic);
		PFlightTypes.add(LBusiness);
		PFlightTypes.add(LEconomic1);
		PFlightTypes.add(LBusiness1);

		LBusiness.setVisible(false);
		LEconomic1.setVisible(false);
		LBusiness1.setVisible(true);
		LEconomic.setVisible(true);

		LDomesticFlight.setBounds(60, 60, 100, 25);
		LInternationalFlight.setBounds(60, 100, 120, 25);

		c.add(PFlightTypes);
		c.add(PLogin);
		c.add(PFlightDetails);

		PFlightTypes.add(LDomesticFlight);
		PFlightTypes.add(LInternationalFlight);

		pack();
		setVisible(true);

		addWindowListener(new ExitListener());

		LDomesticFlight.addMouseListener(new mouse1(this, true));
		LInternationalFlight.addMouseListener(new mouse1(this, false));

		LDomesticFlight1.addMouseListener(new mouse3(this, true));
		LInternationalFlight1.addMouseListener(new mouse3(this, false));

		LBusiness1.addMouseListener(new mouse2(this, true));
		LEconomic1.addMouseListener(new mouse2(this, false));

		Enroll.addActionListener(new buttonEnroll(this));
		BLogin.addActionListener(new buttonLogin(this));
	}

}
class buttonEnroll implements ActionListener {
	String id,pw,f_name,l_name, email, p_number, c_number;
	JFrame FEnroll;
	JPanel panelNewID;
	JLabel labelNewID;
	JTextField NewID;
	// 등록아이디
	JPanel panelNewPW;
	JLabel labelNewPW;
	JPasswordField NewPW;
	// 등록 비밀번호
	JPanel panelFirstName;
	JLabel labelFirstName;
	JTextField FirstName;
	// 등록 이름
	JPanel panelLastName;
	JLabel labelLastName;
	JTextField LastName;
	// 등록 성
	JPanel panelEmail;
	JLabel labelEmail;
	JTextField Email;
	//등록 이메일
	JPanel panelNumber;
	JLabel labelNumber;
	JTextField Number;
	//등록 전화번호
	JPanel panelCard;
	JLabel labelCard;
	JTextField Card;
	//등록 카드번호
	
	JPanel panelButton;
	JButton access;
	// 등록버튼
	LoginPage type;
	
	public buttonEnroll (LoginPage type) {
		
		
		
		this.type = type;
		FEnroll = new JFrame("Enrollment");
		panelNewID = new JPanel();
		labelNewID = new JLabel("ID : ");
		NewID = new JTextField(20);
		// 등록아이디
		panelNewPW = new JPanel();
		labelNewPW = new JLabel("PW : ");
		NewPW = new JPasswordField(20);
		
		panelFirstName = new JPanel();
		labelFirstName = new JLabel("First_Name : ");
		FirstName = new JTextField(20);
		
		panelLastName = new JPanel();
		labelLastName = new JLabel("Last_Name : ");
		LastName = new JTextField(20);
		
		panelEmail = new JPanel();
		labelEmail = new JLabel("E-mail : ");
		Email = new JTextField(20);
		
		panelNumber = new JPanel();
		labelNumber = new JLabel("Phone-number : ");
		Number = new JTextField(11);
		
		panelCard = new JPanel();
		labelCard = new JLabel("Card-number : ");
		Card = new JTextField(16);
		
		panelButton = new JPanel();
		access = new JButton("Enrollment");// "등록"
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
		FEnroll.setBackground(Color.white);
		panelNewID.add(labelNewID);
		panelNewID.add(NewID);
		// 패널ID에 라벨과 버튼을 붙인다.

		panelNewPW.add(labelNewPW);
		panelNewPW.add(NewPW);
		// 패널PW에 라벨과 버튼을 붙인다.
		panelFirstName.add(labelFirstName);
		panelFirstName.add(FirstName);
		// 패널PW에 라벨과 버튼을 붙인다.
		panelLastName.add(labelLastName);
		panelLastName.add(LastName);
		
		panelEmail.add(labelEmail);
		panelEmail.add(Email);
		
		panelNumber.add(labelNumber);
		panelNumber.add(Number);
		
		panelCard.add(labelCard);
		panelCard.add(Card);
		
		panelButton.add(access);
		
		Box NewIDPW = Box.createVerticalBox();
		NewIDPW.add(panelNewID);
		NewIDPW.add(panelNewPW);		
		NewIDPW.add(panelFirstName);
		NewIDPW.add(panelLastName);
		NewIDPW.add(panelEmail);
		NewIDPW.add(panelNumber);
		NewIDPW.add(panelCard);
		NewIDPW.add(panelButton);
		
		FEnroll.add(NewIDPW, BorderLayout.CENTER);

		FEnroll.pack();
		FEnroll.setVisible(true);
		access.addActionListener(new ActionListener() {
			// 회원가입 버튼글릭시 액션
			public void actionPerformed(ActionEvent e) {
				Server_sqlHelper test = new Server_sqlHelper();
				id= NewID.getText();
				pw= NewPW.getText();
				f_name= FirstName.getText();
				l_name = LastName.getText();
				email = Email.getText();
				p_number = Number.getText();
				c_number = Card.getText();
				
				test.enrollment(id, pw, f_name, l_name, email, p_number, c_number);
				
			}
		});
		
	}
	
}
class buttonLogin implements ActionListener {
	LoginPage type;
	boolean isLogin;
	String pw_input;
	String id_input;
	//char[] cCheck, cPassword = { 'v', 'i', 'j', 'a', 'y', '\0' };
	JFrame f;
	//String sCheck, sCheck1 = "vijay";

	public buttonLogin(LoginPage type) {
		this.type = type;
	}
	
	
	public void actionPerformed(ActionEvent e) {
		pw_input = type.TPPassword.getText();
		//password
		id_input = type.TFUserName.getText();
		//id
		Server_sqlHelper test = new Server_sqlHelper();
		isLogin = test.login(id_input, pw_input);
		
		if (isLogin==true) {
			type.PLogin.add(type.LDomesticFlight1);
			type.PLogin.add(type.LInternationalFlight1);

			type.remove(type.LUserName);
			type.PLogin.remove(type.TFUserName);
			type.PLogin.remove(type.LPassword);
			type.PLogin.remove(type.TPPassword);
			type.PLogin.remove(type.BLogin);

			type.c.repaint();
		} else {
			JOptionPane.showMessageDialog(null,
					"Invalid ID or password. Try again");
		}
	}
}

class mouse1 extends MouseAdapter {
	LoginPage type;
	boolean bCheck;

	public mouse1(LoginPage type, boolean bCheck) {
		this.type = type;
		this.bCheck = bCheck;
	}

	public void mouseEntered(MouseEvent e) {
		type.LDomesticFlight.setCursor(Cursor
				.getPredefinedCursor(Cursor.HAND_CURSOR));
		type.LInternationalFlight.setCursor(Cursor
				.getPredefinedCursor(Cursor.HAND_CURSOR));
	}

	public void mouseClicked(MouseEvent e) {
		if (bCheck)
			type.bCheck = true;
		else
			type.bCheck = false;
		type.LEconomic.setVisible(true);
		type.LBusiness1.setVisible(true);
		type.LEconomic1.setVisible(false);
		type.LBusiness.setVisible(false);

		type.JSP1.setVisible(false);
		type.JSP2.setVisible(false);
		type.JSP3.setVisible(false);
		type.JSP4.setVisible(false);
		if (bCheck)
			type.JSP1.setVisible(true);
		else
			type.JSP2.setVisible(true);
	}
}

class mouse3 extends MouseAdapter {
	LoginPage type;
	boolean bCheck;

	public mouse3(LoginPage type, boolean bCheck) {
		this.type = type;
		this.bCheck = bCheck;
	}

	public void mouseEntered(MouseEvent e) {
		type.LDomesticFlight1.setCursor(Cursor
				.getPredefinedCursor(Cursor.HAND_CURSOR));
		type.LInternationalFlight1.setCursor(Cursor
				.getPredefinedCursor(Cursor.HAND_CURSOR));
	}

	public void mouseClicked(MouseEvent e) {
		if (bCheck)
			new DomesticFlight(type);
		else
			new InternationalFlight(type);
	}
}

class mouse2 extends MouseAdapter {
	LoginPage type;
	boolean bCheck;

	public mouse2(LoginPage type, boolean bCheck) {
		this.type = type;
		this.bCheck = bCheck;
	}

	public void mouseEntered(MouseEvent e) {
		type.LEconomic1.setCursor(Cursor
				.getPredefinedCursor(Cursor.HAND_CURSOR));
		type.LBusiness1.setCursor(Cursor
				.getPredefinedCursor(Cursor.HAND_CURSOR));
	}

	public void mouseClicked(MouseEvent e) {
		if (type.bCheck) {
			if (bCheck) {
				type.LBusiness1.setVisible(false);
				type.LBusiness.setVisible(true);
				type.LEconomic.setVisible(false);
				type.LEconomic1.setVisible(true);
				type.JSP1.setVisible(false);
				type.JSP2.setVisible(false);
				type.JSP3.setVisible(true);
				type.JSP4.setVisible(false);
			} else {
				type.LEconomic1.setVisible(false);
				type.LBusiness.setVisible(false);
				type.LBusiness1.setVisible(true);
				type.LEconomic.setVisible(true);
				type.JSP1.setVisible(true);
				type.JSP2.setVisible(false);
				type.JSP3.setVisible(true);
				type.JSP4.setVisible(false);
			}
		} else {
			if (bCheck) {
				type.LBusiness1.setVisible(false);
				type.LBusiness.setVisible(true);
				type.LEconomic.setVisible(false);
				type.LEconomic1.setVisible(true);
				type.JSP1.setVisible(false);
				type.JSP2.setVisible(false);
				type.JSP3.setVisible(false);
				type.JSP4.setVisible(true);
			} else {
				type.LEconomic1.setVisible(false);
				type.LBusiness.setVisible(false);
				type.LBusiness1.setVisible(true);
				type.LEconomic.setVisible(true);
				type.JSP1.setVisible(false);
				type.JSP2.setVisible(true);
				type.JSP3.setVisible(false);
				type.JSP4.setVisible(false);
			}
		}
	}
}
