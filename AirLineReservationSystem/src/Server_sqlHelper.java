import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;

public class Server_sqlHelper {
	private Connection conn;
	private java.sql.Statement stmt;
	private java.sql.ResultSet rs;

	// Connection java.sql.Connection
	public Server_sqlHelper() {
		this.connectionSQL();
	}

	public void connectionSQL() {
		try {
			// 해당 클래스를 메모리에 로드, 해당 클래스가 존재하는지 여부를 확인
			Class.forName("com.mysql.jdbc.Driver");

			//
			// "jdbc:mysql://호스트주소:포트/데이타베이스명","ID","PASSWORD"
			//
			/*
			String url = "jdbc:mysql://localhost:8889/nunagong";
			conn = DriverManager.getConnection(url, "minjun", "1234");
			*/
			 /**
			db_name : 데이터베이스 이름
			root : DBMS 계정
			mysql : DBMS 암호
			*/
			
			conn = DriverManager
					.getConnection("jdbc:mysql://localhost:8889/ars_team3",
							"minjun", "1234");

			System.out.println("데이터베이스 접속 성공");
			// 데이터베이스 객체 메모리에서 소멸
			// conn.close();
		} catch (ClassNotFoundException e) {
			System.out.println("드라이버가 존재하지 않습니다." + e);
		} catch (Exception e) {
			System.out.println("오류: " + e);
		}
	}

	public String[][] getFlightInfo() {
		String attribute[] = new String[6];
		int tuple_count = 0;
		String tuple[][] = null;

		String createString;
		createString = "create table Employees (" +
							"Employee_ID INTEGER, " +
							"Name VARCHAR(30))";
		
		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery(createString);
			rs.beforeFirst();
			while (rs.next()) {
				tuple_count++;
			}

			tuple = new String[tuple_count][6];

			rs.beforeFirst();
			for (int i = 0; i < tuple_count; i++) {
				rs.next();
				for (int j = 0; j < attribute.length; j++) {
					attribute[0] = rs.getString("Departure");
					attribute[1] = rs.getString("Arrival");
					attribute[2] = rs.getString("DepartureTime");
					attribute[3] = rs.getString("ArrivalTime");
					attribute[4] = rs.getString("Price");
					attribute[5] = rs.getString("Airline");
					tuple[i][j] = attribute[j];
				}
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return tuple;
	}

	public boolean login(String ID, String password) {
		boolean isLogin = false;
		String attribute[] = new String[2];
		String db_ID_PW[][] = null;
		int tuple_count = 0;

		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery("select * from member");
			rs.beforeFirst();

			while (rs.next()) {
				tuple_count++;
			}
			db_ID_PW = new String[tuple_count][2];

			rs.beforeFirst();
			for (int i = 0; i < tuple_count; i++) {
				rs.next();
				for (int j = 0; j < 2; j++) {
					attribute[0] = rs.getString("ID");
					attribute[1] = rs.getString("PASSWORD");
					db_ID_PW[i][j] = attribute[j];
					System.out.print(db_ID_PW[i][j]);

				}
				System.out.println();
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// 아이디 비번 체크
		for (int i = 0; i < tuple_count; i++) {

			if (ID.equals(db_ID_PW[i][0])) {
				if (password.equals(db_ID_PW[i][1])) {
					isLogin = true;
					break;
				}
			}
		}

		return isLogin;
	}

	public void enrollment(String id, String pw, String f_name, String l_name,
			String email, String p_number, String c_number) {
		try {
			
			stmt = conn.createStatement();
			rs = stmt
					.executeQuery("insert into `ars_team3`.`member` (`ID`, `password`, `firstname`, `lastname`, `email`, `phone`) values ('1', '1', '1', '1', '11', '11')");
//			rs = stmt
//					.executeQuery("INSERT INTO member (ID,password,firstname,lastname,email,phone,card) "
//							+ "VALUES"
//							+ "("
//							+ id
//							+ ","
//							+ pw
//							+ ","
//							+ f_name
//							+ ","
//							+ l_name
//							+ ","
//							+ email
//							+ ","
//							+ p_number
//							+ ","
//							+ c_number+")");
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void main(String[] args) {
		String[][] test1;
		boolean test2;
		Server_sqlHelper test = new Server_sqlHelper();
		test1 = test.getFlightInfo();
		test2 = test.login("hgbg", "1149811kk");
		test.enrollment("1", "2", "3", "4", "5", "6", "7");
		System.out.println(test2);
		// for (int i = 0; i < test1.length; i++) {
		// for (int j = 0; j < test1[i].length; j++) {
		// System.out.print(test1[i][j] + " " + i + "," + j + " ");
		// }
		// System.out.println();
		// }

		// Connection 레퍼런스 Type은 java.sql.Connection 클래스

	}

}