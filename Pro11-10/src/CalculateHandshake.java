/**
 * Program:11-10 Calculate how many time n people handshaked. Author:Choi Min
 * Jun Last Changed: 05/06/2014
 * 
 * @author MINJUN
 */
/**
 * Counting N Peoples handshake is nC2.
 */
import java.util.Scanner;

public class CalculateHandshake {
	public static void main(String[] args) {
		int n;
		Scanner keyboard = new Scanner(System.in);
		do {
			System.out.println("Put how many pepoles exist to handshake.");
			n = keyboard.nextInt();
		} while (n < 2);

		int result = (getHandshaked(n) / getHandshaked(n - 2)) / 2;
		System.out.println(n + " People needs to handshake " + result
				+ " times.");
	}

	public static int getHandshaked(int n) {
		if (n == 1)
			return 1;
		else
			return n * getHandshaked(n - 1);
	}
}
