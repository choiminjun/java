/**
 * Choi Min Jun
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class CountLetter {
	public static void main(String[] args) {
		Scanner in = null;

		int count = 0;

		String find_char = args[1];
		try {
			in = new Scanner(new File(args[2]));
			while (in.hasNextLine()) {
				String line = in.nextLine();
				for (int index = 0; index < line.length(); index++) {
					if ((index + 1) > line.length()) // This prohibits from
														// array out of bounds.
						continue;
					String temp = line.substring(index, index + 1);
					if (temp.equalsIgnoreCase(find_char))
						count++;
				}
			}
			System.out.println("Count: " + count);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
