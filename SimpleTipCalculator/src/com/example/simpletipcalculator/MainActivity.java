package com.example.simpletipcalculator;


import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity implements OnCheckedChangeListener  {

	TextView Name;
	EditText Price_Input;
									// which gets other rate of tax.
	RadioButton rb1, rb2, rb3;
	EditText rb3_Input;
	TextView result; // result that show adding TAX and PRICE.
	Button btn;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		//Name = (TextView) findViewById(R.id.LabelName);
		Price_Input = (EditText) findViewById(R.id.value);
		rb1 = (RadioButton)findViewById(R.id.radio01);
		rb1.setOnCheckedChangeListener(this);
		rb2 = (RadioButton)findViewById(R.id.radio02);
		rb2.setOnCheckedChangeListener(this);
		rb3 = (RadioButton)findViewById(R.id.radio03);
		rb3.setOnCheckedChangeListener(this);
		
		rb3_Input = (EditText) findViewById(R.id.radio03_Text);
		result = (TextView) findViewById(R.id.radio03_Text);
		btn.setOnClickListener(new OnClickListener(){
			public void onClick(View v){
				display();
			}
		});
	}
	
	public String display(){
		String price =  Price_Input.getText().toString();
		String rate ="";
		int tip_rate;
		double tip;
		double total;			// price + tax.
		if(rb1.isChecked())
			rate = rb1.getText().toString().substring(0, 1);
		if(rb2.isChecked())
			rate = rb2.getText().toString().substring(0, 1);
		if(rb3.isChecked())
			rate = rb3_Input.getText().toString();
		tip_rate = Integer.parseInt(rate);
		
	    tip = Integer.parseInt(price) * (double)(tip_rate/100.0);  //calculating tip.
	    String display = "Tip : " + Double.toString(tip);
	    Toast.makeText(this, display, Toast.LENGTH_LONG).show();
	    return display;
	}

	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		;
		/*
		if (radio.getCheckedRadioButtonId() == R.id.radioButton1)
			Toast.makeText(this, "∞°¿ß", Toast.LENGTH_SHORT).show();
		else if (radio.getCheckedRadioButtonId() == R.id.radioButton2)
			Toast.makeText(this, "πŸ¿ß", Toast.LENGTH_SHORT).show();
		else if (radio.getCheckedRadioButtonId() == R.id.radioButton3)
			Toast.makeText(this, "∫∏", Toast.LENGTH_SHORT).show();
*/
	}
}
