/**
 Program displaying grade.
 Author : Choi Min Jun.
 Email Address: choiminjun0720@gmail.com
 Programming Projects 3 - 2.
 Last Changed : 26/03/2014.
 */
import java.util.Scanner;

//Using ArrayList is for studying java more deeply.
public class ReadNonNegativeNumber {
	public static void main(String[] args){
		int count = 0;		//for check wheather positive Integers are three.
		int arr[] = new int[3];
		Scanner keyboard = new Scanner(System.in);
		while(count < 3	){
			System.out.println("Put non-negative Integer :");
			int num = keyboard.nextInt();
			if(num >= 0){	//if num is nonnegative
				arr[count] = num;
				count++;
			}
			else
				System.out.println("PUT Positive!");
		}
		for(int i = 0 ; i < arr.length - 1 ; i++){
			int indexOfNextSmallest = getIndexOfSmallest(i , arr);
			interchange(i , indexOfNextSmallest , arr);
		}
		for(int i = 0 ; i < arr.length ; i++)
			System.out.println(arr[i] + " ");
	}
	/**
	 * get indexof smallest in array and return index which is
	 * used by interchange method.
	 */
	private static int getIndexOfSmallest(int startIndex , int [] a){
		int min = a[startIndex];
		int indexOfMin = startIndex;
		for(int i = startIndex ; i < a.length ; i++){
			if(a[i] < min){
				min = a[i];
				indexOfMin = i; 
			}
		}
		return indexOfMin;
	}
	private static void interchange(int i , int j , int[] a){
		int temp = a[i];
		a[i] = a[j];
		a[j] = temp;
	}
}
