public class Point3D extends Point {
	int z;

	public Point3D() {
		super();
		this.z = 0;
	}

	public Point3D(int x, int y, int z) {
		super(x, y);
		this.z = z;
		//z = z;
	}

	public int getZ() {
		return this.z;
	}

	public void setZ(int z) {
		this.z = z;
	}
/**
 * super 는 Overriding 시켰을때, 조상의 mehtod를 부르고 싶을때 사용한다.
 */
	public String toString() {
		String str = "(" + getX() + "," + super.getY() + "," + this.z
				+ ")";
		return str;
	}
}
