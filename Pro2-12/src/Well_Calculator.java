/**
 Program calculates gallon in Well
 Author : Choi Min Jun.
 Email Address: choiminjun0720@gmail.com
 Programming Projects 2 - 12.
 Last Changed : 15/03/2014.
 */
/**
 * First this problem is little hard to translate, and 
 * the meaning of cubic foot is ambiguous.
 * So I assumed cubic foot as volume and solver problem.
 * In programming project 2-12's problem, it says that
 * when 300 foot-well with radius 3inches, it say casing holds about 441gallons
 * so first program will calculate rate at gallons per volume.
 * 
 */
import java.util.Scanner;

public class Well_Calculator {
	public static void main(String[] args) {
		double pi = 3.14;
		double h, r; // height and radius..
		double gallons; // output.
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Input height and radius.");
		h = keyboard.nextDouble();
		r = keyboard.nextDouble();
		double volume = pi * r * r * h;

		double rate = (double) 300 * 3.14 * 3*3 / 441; // using problems sample.
		// problem say that when h is 300 and r is 3, they say
		// 441 gallons, so using this rate
		gallons = volume / rate;
		System.out.println("The gallon of " + h + " height and " + r
				+ " radius are " + gallons);
		judging(gallons);
	}
//judging wheather there are plenty waters or not.
	public static void judging(double num) {
		if (num >= 250)
			System.out.println("There is Plenty water for family.");
		else
			System.out.println("There needs more water for family");
	}
}
