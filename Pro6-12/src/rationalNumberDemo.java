/**
 * Author : Choi Min Jun 
 * Pro6-12
 * Last Changed : April. 28. 2014
 */

import java.util.Scanner;

public class rationalNumberDemo {
	public static void main(String[] args) {
		int a, b; //a is divided by b.
		double rationalResult; //result of rational.
		String rationalResult_s;
		
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Please enter the number 'a' : ");
		a = keyboard.nextInt();// initializing.
		System.out.println("Please enter the number 'b' : ");
		b = keyboard.nextInt();
		
		//initializing by using constructor.
		rationalNumber test_Constructor = new rationalNumber(a, b);
		rationalResult = test_Constructor.getValue();
		rationalResult_s = test_Constructor.toString();
	
		//print out result.
		System.out.println("Double type RationalNumber is " + rationalResult);
		System.out.println("String type RationalNumber is " + rationalResult_s);
	}
}
