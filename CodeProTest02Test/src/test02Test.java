import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>test02Test</code> contains tests for the class <code>{@link test02}</code>.
 *
 * @generatedBy CodePro at 14. 5. 27 오후 8:17
 * @author MINJUN
 * @version $Revision: 1.0 $
 */
public class test02Test {
	/**
	 * Run the int add(int,int,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 5. 27 오후 8:17
	 */
	@Test
	public void testAdd_1()
		throws Exception {
		test02 fixture = new test02();
		int a = 3;
		int b = 1;
		int c = 1;

		int result = fixture.add(a, b, c);

		// add additional test code here
		assertEquals(5, result);
	}

	/**
	 * Run the double divide(double,double) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 5. 27 오후 8:17
	 */
	@Test
	public void testDivide_1()
		throws Exception {
		test02 fixture = new test02();
		double a = 20;
		double b = 10;

		double result = fixture.divide(a, b);

		// add additional test code here
		assertEquals(2, result, 0.1);
	}

	/**
	 * Run the int multply(int,int,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 5. 27 오후 8:17
	 */
	@Test
	public void testMultply_1()
		throws Exception {
		test02 fixture = new test02();
		int a = 9;
		int b = 3;
		int c = 1;

		int result = fixture.multply(a, b, c);

		// add additional test code here
		assertEquals(27, result);
	}

	/**
	 * Run the int subtract(int,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 5. 27 오후 8:17
	 */
	@Test
	public void testSubtract_1()
		throws Exception {
		test02 fixture = new test02();
		int a = 5;
		int b = 5;

		int result = fixture.subtract(a, b);

		// add additional test code here
		assertEquals(0, result);
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 14. 5. 27 오후 8:17
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 14. 5. 27 오후 8:17
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 14. 5. 27 오후 8:17
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(test02Test.class);
	}
}