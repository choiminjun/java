import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>MazeTest</code> contains tests for the class <code>{@link Maze}</code>.
 *
 * @generatedBy CodePro at 14. 6. 16 오후 3:51
 * @author MINJUN
 * @version $Revision: 1.0 $
 */
public class AirlineTest {
	/**
	 * Run the Maze(int,int) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 16 오후 3:51
	 */
	@Test
	public void testMaze_1()
		throws Exception {
		int row = 1;
		int col = 1;

		Maze result = new Maze(row, col);

		// add additional test code here
		assertNotNull(result);
	}

	/**
	 * Run the void dfs(int,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 16 오후 3:51
	 */
	@Test
	public void testDfs_1()
		throws Exception {
		Maze fixture = new Maze(1, 1);
		fixture.dest_y = 1;
		fixture.dest_x = 1;
		fixture.maze = new int[][] {};
		fixture.check_visited = new int[][] {};
		int row = -1;
		int col = 1;

		fixture.dfs(row, col);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Can not set [[I field Maze.maze to [I
	}

	/**
	 * Run the void dfs(int,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 16 오후 3:51
	 */
	@Test
	public void testDfs_2()
		throws Exception {
		Maze fixture = new Maze(1, 1);
		fixture.dest_y = 1;
		fixture.dest_x = 1;
		fixture.maze = new int[][] {};
		fixture.check_visited = new int[][] {};
		int row = 1;
		int col = -1;

		fixture.dfs(row, col);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Can not set [[I field Maze.maze to [I
	}

	/**
	 * Run the void dfs(int,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 16 오후 3:51
	 */
	@Test
	public void testDfs_3()
		throws Exception {
		Maze fixture = new Maze(1, 1);
		fixture.dest_y = 1;
		fixture.dest_x = 1;
		fixture.maze = new int[][] {};
		fixture.check_visited = new int[][] {};
		int row = 1;
		int col = 1;

		fixture.dfs(row, col);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Can not set [[I field Maze.maze to [I
	}

	/**
	 * Run the void dfs(int,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 16 오후 3:51
	 */
	@Test
	public void testDfs_4()
		throws Exception {
		Maze fixture = new Maze(1, 1);
		fixture.dest_y = 1;
		fixture.dest_x = 1;
		fixture.maze = new int[][] {};
		fixture.check_visited = new int[][] {};
		int row = 1;
		int col = 1;

		fixture.dfs(row, col);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Can not set [[I field Maze.maze to [I
	}

	/**
	 * Run the void dfs(int,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 16 오후 3:51
	 */
	@Test
	public void testDfs_5()
		throws Exception {
		Maze fixture = new Maze(1, 1);
		fixture.dest_y = 1;
		fixture.dest_x = 1;
		fixture.maze = new int[][] {};
		fixture.check_visited = new int[][] {};
		int row = 1;
		int col = 1;

		fixture.dfs(row, col);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Can not set [[I field Maze.maze to [I
	}

	/**
	 * Run the void dfs(int,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 16 오후 3:51
	 */
	@Test
	public void testDfs_6()
		throws Exception {
		Maze fixture = new Maze(1, 1);
		fixture.dest_y = 1;
		fixture.dest_x = 1;
		fixture.maze = new int[][] {};
		fixture.check_visited = new int[][] {};
		int row = 1;
		int col = 1;

		fixture.dfs(row, col);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Can not set [[I field Maze.maze to [I
	}

	/**
	 * Run the void dfs(int,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 16 오후 3:51
	 */
	@Test
	public void testDfs_7()
		throws Exception {
		Maze fixture = new Maze(1, 1);
		fixture.dest_y = 1;
		fixture.dest_x = 1;
		fixture.maze = new int[][] {};
		fixture.check_visited = new int[][] {};
		int row = 1;
		int col = 1;

		fixture.dfs(row, col);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Can not set [[I field Maze.maze to [I
	}

	/**
	 * Run the void dfs(int,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 16 오후 3:51
	 */
	@Test
	public void testDfs_8()
		throws Exception {
		Maze fixture = new Maze(1, 1);
		fixture.dest_y = 1;
		fixture.dest_x = 1;
		fixture.maze = new int[][] {};
		fixture.check_visited = new int[][] {};
		int row = 1;
		int col = 1;

		fixture.dfs(row, col);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Can not set [[I field Maze.maze to [I
	}

	/**
	 * Run the void dfs(int,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 16 오후 3:51
	 */
	@Test
	public void testDfs_9()
		throws Exception {
		Maze fixture = new Maze(1, 1);
		fixture.dest_y = 1;
		fixture.dest_x = 1;
		fixture.maze = new int[][] {};
		fixture.check_visited = new int[][] {};
		int row = 1;
		int col = 1;

		fixture.dfs(row, col);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Can not set [[I field Maze.maze to [I
	}

	/**
	 * Run the void display() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 16 오후 3:51
	 */
	@Test
	public void testDisplay_1()
		throws Exception {
		Maze fixture = new Maze(1, 1);
		fixture.dest_y = 1;
		fixture.dest_x = 1;
		fixture.maze = new int[][] {};
		fixture.check_visited = new int[][] {};

		fixture.display();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Can not set [[I field Maze.maze to [I
	}

	/**
	 * Run the void display() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 16 오후 3:51
	 */
	@Test
	public void testDisplay_2()
		throws Exception {
		Maze fixture = new Maze(1, 1);
		fixture.dest_y = 1;
		fixture.dest_x = 1;
		fixture.maze = new int[][] {};
		fixture.check_visited = new int[][] {};

		fixture.display();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Can not set [[I field Maze.maze to [I
	}

	/**
	 * Run the void display() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 16 오후 3:51
	 */
	@Test
	public void testDisplay_3()
		throws Exception {
		Maze fixture = new Maze(1, 1);
		fixture.dest_y = 1;
		fixture.dest_x = 1;
		fixture.maze = new int[][] {};
		fixture.check_visited = new int[][] {};

		fixture.display();

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.IllegalArgumentException: Can not set [[I field Maze.maze to [I
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 14. 6. 16 오후 3:51
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 14. 6. 16 오후 3:51
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 14. 6. 16 오후 3:51
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(AirlineTest.class);
	}
}