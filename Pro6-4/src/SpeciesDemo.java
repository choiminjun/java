/**
 * Author : Choi Min Jun
 * Programming Pro 6-2
 * Last Changed : April. 28. 2014
 */

public class SpeciesDemo {
	public static void main(String[] args)
	{
		Species test = new Species("minjun", 80, 8);
		test.writeOutput();
		System.out.println("10 years late population :  " + test.predictPopulation(10));
	}
}
