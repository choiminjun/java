/**
 * Program:12-10 Linked List.
 *  Author:Choi Min Jun 
 *  Last Changed: 06/06/2014
 * @author MINJUN
 */
import java.util.Scanner;
public class ObjectLinkedListSelfContainedDemo {
	public static void main(String[] args){
		ObjectLinkedListSelfContained list = new ObjectLinkedListSelfContained();
		Scanner keyboard = new Scanner(System.in);
		String check = "y";
		/**
		 * Inserting Employee objects in linkedlist.
		 */
		do{
			System.out.println("Put Emloyess id, name, department, expLevel");
			int id = keyboard.nextInt();
			String name = keyboard.next();
			String department = keyboard.next();
			double expLevel = keyboard.nextDouble();
			Employee newEmployee = new Employee(id,name,department,expLevel);
			list.addANodeToStart(newEmployee);
			
			System.out.println("You want Insert more?");
			check = keyboard.next();
		}while(check.equalsIgnoreCase("y"));
		
		list.showList();
		/**
		 * Find Employee object with social security number.
		 */
		
		check = "y";
		do{
			System.out.println("Put Employee's Id number to find");
			int targetSocialNumber = keyboard.nextInt();
			Employee result = list.find(targetSocialNumber);
			if(result != null){
				String findResult = result.toString();
				System.out.println(findResult);
			}
			else
				System.out.println("Finding failed!!.");
			
			System.out.println("You want more to find?");
			check = keyboard.next();
		}while(check.equalsIgnoreCase("y"));
	}
}
