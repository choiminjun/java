/**
 * Program:11-3 Calculating factorial.
 *  Author:Choi Min Jun 
 *  Last Changed: 05/06/2014
 * @author MINJUN
 */
import java.util.Scanner;

public class Factorial {
	public static void main(String[] args) {
		int n = 0;
		Scanner keyboard = new Scanner(System.in);

		do {
			System.out.println("Put Factorial n to calculate");
			n = keyboard.nextInt();
			if(n>=0){
				System.out.println(n + "! is equal to ");
				int result = factorial(n);
				System.out.println("Result : " + result);
			}
			else{ //whe n < 0
				System.out.println("Negative can't be input.");
			}
		} while (n >= 0);

		
	}

	public static int factorial(int n) {
		if (n == 1) {
			System.out.print(n);
			return 1;
		} else {
			System.out.print(n + " x ");
			return n * factorial(n - 1);
		}
	}
}
