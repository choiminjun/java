public class Truck extends Vehicle {
	private double loadCapacity;
	private double towingCapacity;
//constructor..
	public Truck() {
		super();
		loadCapacity = 0;
		towingCapacity = 0;
	}

	public Truck(double loadCapacity, double towingCapacity) {
		super();
		this.loadCapacity = loadCapacity;
		this.towingCapacity = towingCapacity;
	}

	public Truck(String ownerName, String manufactureName,
			int numberOfCylinders, double loadCapacity, double towingCapacity) {
		super(ownerName, manufactureName, numberOfCylinders);
		this.loadCapacity = loadCapacity;
		this.towingCapacity = towingCapacity;
	}

	public String getOwnerName() {
		return super.getOwnerName();
	}
/**
 * Compare two trucks condition.
 * @param otherTruck
 * @return
 */
	public boolean equals(Truck otherTruck) {
		if (super.getOwnerName().equalsIgnoreCase(otherTruck.getOwnerName())
				&& super.getManufactureName().equalsIgnoreCase(
						otherTruck.getManufactureName())
				&& super.getNumberOfCylinders() == otherTruck
						.getNumberOfCylinders()
				&& this.loadCapacity == otherTruck.loadCapacity
				&& this.towingCapacity == otherTruck.towingCapacity)
			return true;
		else
			return false;
	}
}
