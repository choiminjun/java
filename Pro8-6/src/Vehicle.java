
public class Vehicle {
	private String manufactureName;
	private int numberOfCylinders;
	Person owner = new Person(); //owner.
	
	public Vehicle(){
		manufactureName = null;
		numberOfCylinders = 0;
	}
	
	public Vehicle(String manufactureName){
		this.manufactureName = manufactureName;
	}
	public Vehicle(int numberOfCylinders){
		this.numberOfCylinders = numberOfCylinders;
	}
	public Vehicle(String manufactureName, int numberOfCylinders){
		this.manufactureName = manufactureName;
		this.numberOfCylinders = numberOfCylinders;
	}
	public Vehicle(String ownerName, String manufactureName, int numberOfCylinders){
		owner.setName(ownerName);
		this.manufactureName = manufactureName;
		this.numberOfCylinders = numberOfCylinders;
	}
	/**
	 * This methods returns ownername for Truck classes.
	 * @return
	 */
	public String getOwnerName(){
		return owner.getName();
	}
	public String getManufactureName(){
		return manufactureName;
	}
	public int getNumberOfCylinders(){
		return numberOfCylinders;
	}
	
}
