
public class Person {
	private String name;
	private int age;
	/**
	 * using default constructor.
	 */
	public Person(){
		this.name ="";
		this.age = 0;
	}
	/**
	 * using constructor for setup values instead of using set method.
	 */
	public Person(String temp_name, int temp_age ){
		this.name = temp_name;
		this.age = temp_age;
	}
	
	public String getName(){
		return name; 		//return name.
	}
	public int getAge(){
		return age;			//return age.
	}

}
