
public class test03 {
	public int[] sort(int[] arr) {/*@Start*/
		for (int i = 1; i < arr.length; i++) {
			int standard = arr[i]; // get standard value for compare.
			int standard_index = i - 1; // standard index of comparing

			while (standard_index >= 0 && standard < arr[standard_index]) {
				int temp = arr[standard_index + 1];
				arr[standard_index + 1] = arr[standard_index];
				arr[standard_index] = temp;;
				standard_index--;
			}
			arr[standard_index + 1] = standard; // save standard value.
		}
		/*@End*/
		return arr;/*@Return*/
	}

}
