import java.util.Scanner;

public class DivideByZeroDemo extends Exception {
	private int numerator;
	private int denominator;
	private double quotient;

	public class DivideByZeroException extends Exception {
		public DivideByZeroException() {
			super("Dividing by Zero!");
		}

		public DivideByZeroException(String message) {
			super(message);
		}
	}

	// public class ExceptionDivide extends Exception {};

	public static void main(String[] args) {
		try{
		DivideByZeroDemo oneTime = new DivideByZeroDemo();
		oneTime.doIt();
		}catch(DivideByZeroException e){
			System.out.println(e.getMessage());
		}
	}
/**
 * 여기서 throws DivideByZeroException은 이 method를 호출한 함수에게 자신이 throw를 할테니
 * 호출한 method에서 error처리 할 것을 준비하라는 의미이다.
 * @throws DivideByZeroException
 */
	public void doIt() throws DivideByZeroException {
		
			System.out.println("Enter numerator:");
			Scanner keyboard = new Scanner(System.in);
			numerator = keyboard.nextInt();
			System.out.println("Enter denominator:");
			denominator = keyboard.nextInt();
			/**
			 * 여기서 try catch로 surrounding 안하고 사용하는 것도 중요하다.
			*/
			if (denominator == 0) {				
				throw new DivideByZeroException();
			}
			quotient = numerator / (double) denominator;
			System.out
					.println(numerator + "/" + denominator + " = " + quotient);
			System.out.println("End of program.");
	}

	public void giveSecondChance() {
		try {
			System.out.println("Try again:");
			System.out.println("Enter numerator:");
			Scanner keyboard = new Scanner(System.in);
			numerator = keyboard.nextInt();
			System.out.println("Enter denominator:");
			System.out.println("Be sure the denominator is not zero.");
			denominator = keyboard.nextInt();
			if (denominator == 0) {
				throw new DivideByZeroException();
				/**
				 * System.out.println("I cannot do division by zero.");
				 * System.out.println("Since I cannot do what you want,");
				 * System.out.println("the program will now end.");
				 * 
				 * System.exit(0);
				 */
			}
			quotient = ((double) numerator) / denominator;
			System.out
					.println(numerator + "/" + denominator + " = " + quotient);
		} catch (DivideByZeroException e) {
			System.out.println(e.getMessage());
			// System.out.println(e.printStackTrace());
		}
	}
}