import java.io.Serializable;


public class Employee implements Serializable{
	static private int number = 0;
	
	public String name;
	public int SSN;
	public String address;
	
	private int id;
	
	public Employee(String name, int SSn, String address){
		this.name = name;
		this.SSN = SSN;
		this.address = address;
		id = ++number;
	}
	public int getID(){
		return id;
	}
	public String toString(){
		return "Name: " + name + "(SSN:" + SSN + ")" + "\nAddress";
	}
	public void mailCheck(){
		System.out.println("Mailing a check to " + name + " " + address);
	}
}
