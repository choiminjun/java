import java.io.EOFException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class DeserializeDemo {
	public static void main(String[] args) {
		Employee[] e = null;
		try {
			FileInputStream fileIn = new FileInputStream("employee.dat");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			try {
				int count = 0;
				e = (Employee[]) in.readObject();
				while (true) {
					
					
					System.out.println("Deserialized Employee...");
					System.out.println("Name: " + e[count].name);
					System.out.println("Address: " + e[count].address);
					System.out.println("SSN: " + e[count].SSN);
					System.out.println("ID: " + e[count].getID());
					count++;
				}
			} catch (EOFException i) {
				in.close();
				fileIn.close();
			}
		} catch (IOException i) {
			i.printStackTrace();
			return;
		} catch (ClassNotFoundException c) {
			System.out.println("Employee class not found");
			c.printStackTrace();
			return;
		}
		catch(Exception e1){
			;
		}
	}

}
