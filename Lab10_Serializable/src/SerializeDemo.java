import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;


public class SerializeDemo {
	public static void main(String[] args){
		//array로 3개 잡고 넣고 빼낼때 array로.
		/*
		Employee e1 = new Employee("James", 12345, "Seoul");
		Employee e2 = new Employee("Babari", 78910, "Jeju");
		Employee e3 = new Employee("Richard", 22222, "Incheon");
		*/
		Employee[] e = new Employee[3];
		e[0] = new Employee("James", 12345,"Seoul");
		e[1] = new Employee("Babari", 78910, "Jeju");
		e[2] = new Employee("Richard", 22222, "Incheon");
		try{
			FileOutputStream fileOut = new FileOutputStream("employee.dat");
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			/*
			out.writeObject(e1);
			out.writeObject(e2);
			out.writeObject(e3);
			*/
			out.writeObject(e);
			out.close();
			fileOut.close();
			System.out.printf("Serialized data is saved in employee.dat");
		}catch(IOException i){
			i.printStackTrace();
		}
	}
}
