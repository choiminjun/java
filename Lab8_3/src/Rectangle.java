
public class Rectangle extends Shape {
	int length, width;
	public Rectangle(){
		super();
		this.length = 0;
		this.width = 0;
	}
	public Rectangle(String color,int length, int width){
		super(color);
		this.length = length;
		this.width = width;
	}
	public double getArea(){
		return length * width;
	}
	public String toString(){
		String str = "("+this.color + "," + this.length + "," + this.width + ")";
		return str;
		//colors can be calles super.toString();
	}
}
