
public class Shape {

	String color;
	public Shape(){
		this.color = "NONE";
	}
	public Shape(String color){
		this.color = color;
	}
	public double getArea(){
		return 0;
	}
	public String toString(){
		String str = "("+this.color+ ")";
		return str;
	}
}
