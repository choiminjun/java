public class Basketball {
	private static String team1 = "a"; // initializing team name.
	private static String team2 = "b";
	private static int scoreTeam1 = 0;
	private static int scoreTeam2 = 0;
	public static int test = 1;
	int test2=3;
	/**
	 * return teamname and score.
	 */
	public String getTeam1() {
		return team1;
	}

	public String getTeam2() {
		return team2;
	}

	public int getscoreTeam1() {
		return scoreTeam1;
	}

	public int getscoreTeam2() {
		return scoreTeam2;
	}
     
	/**
	 * update score using method variables which impliest teamname
	 */
	public void updateScoreTeam(String team, int num) {
		if (team.equalsIgnoreCase(team1)) {
			scoreTeam1 += num;
			System.out.println();
		} else { // else if team2
			scoreTeam2 += num;
		}
	}

	/**
	 * checking which team are winner team or equal
	 */
	public String checkWinner() {
		if (scoreTeam1 > scoreTeam2)
			return team1;
		else if (scoreTeam1 < scoreTeam2)
			return team2;
		else
			return "EQUAL!!";
	}

	/**
	 * pre/pro conditions method.
	 */
	public boolean checkCondition(int score) {
		if (score > 3)
			return false;
		else
			return true;
	}

}
