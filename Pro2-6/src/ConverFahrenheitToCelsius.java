/**
 Program to display each number of four-digit numbers.
 Author : Choi Min Jun.
 Email Address: choiminjun0720@gmail.com
 Programming Projects 2 - 6.
 Last Changed : 15/03/2014.
 */
import java.util.Scanner;
public class ConverFahrenheitToCelsius {
	public static void main(String[] args){
		System.out.println("Enter a temperature in degrees Fahrenheit: ");
		Scanner keyboard = new Scanner(System.in);
		int fahrenheit = keyboard.nextInt();
		double celsius = ( 5 * ((double)fahrenheit - 32) )/ 9;  //calculate.
		String display= new java.text.DecimalFormat("#.#").format(celsius);
		System.out.println(fahrenheit + " degrees Fahrenheit is " + display +" degrees Celsius.");
	}
}
