
public class Doctor extends Person{
	private double office_visit_fee;		//fee.
	public Doctor(){
		office_visit_fee = 0;
	}
	public Doctor(String name){
		super(name);
	}
	public Doctor(String name, double fee){
		super(name);
		office_visit_fee = fee;
	}
	public String getName(){
		return super.getName();
	}
	public double getFee(){
		return office_visit_fee;
	}
	/**
	 * compare office visit fee with two object.
	 * @param otherDoctor
	 * @return
	 */
	public boolean equals(Doctor otherDoctor){
		if(this.getName().equalsIgnoreCase(otherDoctor.getName()) ||
				this.office_visit_fee == otherDoctor.getFee())
			return true;
		else 
			return false;
	}
	
}
