/**
 Program displays min,max and average of number.
 Author : Choi Min Jun.
 Email Address: choiminjun0720@gmail.com
 Programming Projects 4 - 5.
 Last Changed : 26/03/2014.
 */
import java.util.ArrayList;
import java.util.Scanner;

 //Using ArrayList is for studying java more deeply.
 
public class DisplayNumbers {
	public static void main(String[] args) {
		ArrayList<Integer> arr = new ArrayList<Integer>();
		Scanner keyboard = new Scanner(System.in);
		int num;
		try {
			do {
				System.out.println("Put nonnegative number :");
				num = keyboard.nextInt();
				if (num >= 0)
					arr.add(num);
				else
					System.out.println("End");
			} while (num >= 0);

			int max = getMax(arr);
			int min = getMin(arr);
			double average = getAvr(arr);

			System.out.println("Max is " + max);
			System.out.println("Min is " + min);
			System.out.println("Average is " + average);
		} catch (RuntimeException e) {
			System.out.println("Memory allocation error");
		}
	}

	public static int getMax(ArrayList<Integer> arr) {
		int max = arr.get(0);
		for (int i = 1; i < arr.size(); i++) {
			if (max < arr.get(i))
				max = arr.get(i);
		}
		return max;
	}

	public static int getMin(ArrayList<Integer> arr) {
		int min = arr.get(0);
		for (int i = 1; i < arr.size(); i++) {
			if (min > arr.get(i))
				min = arr.get(i);
		}
		return min;
	}

	public static double getAvr(ArrayList<Integer> arr) {
		int sum = 0;
		for (int i = 0; i < arr.size(); i++)
			sum += arr.get(i);

		return (double) sum / (double) arr.size();		//for typecasting
	}
}
