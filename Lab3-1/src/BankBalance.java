import java.util.Scanner;
/**
 *		static은 일반적인 다른변수가 매번 새로운 값을 가져오는것과달리
 *		static이라고 선언을 해 놓으면 값을 불러올때 기존 메모리영역에서 기존의 값을 가져와
 *		전역변수와 같은 효과를 생성하는 것이다.
 * @author MINJUN
 */
public class BankBalance {
	public static final double OVERDRAWN_PENALTY = 8.00;
	public static final double INTEREST_RATE = 0.02;
	public static void main(String[] args){
		double balance;
		System.out.println("Enter your checking account balance: $");
		Scanner keyboard = new Scanner(System.in);
		balance = keyboard.nextDouble();
		System.out.println("Original balance $ " + balance);
		
		if(balance >=0)
			balance = balance + (INTEREST_RATE * balance);
		else
			balance = balance - OVERDRAWN_PENALTY;
		
		System.out.println("After adjusting for one month ");
		System.out.println("of interest and penalties,");
		System.out.println("you new balace is $ " + balance);
	}
}
