/**
 Program Sudoku Puzzle.
 Author : Choi Min Jun.
 Email Address: choiminjun0720@gmail.com
 Programming Projects 7 - 12.
 Last Changed : 09/05/2014.
 */
import java.util.Scanner;

public class SudokuPuzzleDemo {
	/** display and checkpuzzle and print result
	 play again.
	 */
	public static void main(String[] args) {
		SudokuPuzzle s = new SudokuPuzzle();
		s.init(); // call initalize method in constructor.
		System.out.println("Sudok Start!!");
		String print_board = s.toString();
		System.out.print(print_board);
		Scanner keyboard = new Scanner(System.in);
		
		while (s.isFull() != true) {
			int row, col, value;
			System.out.println("Put row, col, value: Puzzle is based on (0,0)");
			row = keyboard.nextInt();
			col = keyboard.nextInt();
			value = keyboard.nextInt();
			//check wheather input's are right.
			if (check_condition(row, col, value) == true) {
				/**
				 * add value to board, but this can be temporal
				 * because it does not set true for boolean 
				 * attribute start[row][col].
				 */
				s.addGuess(row - 1, col - 1, value); 
				System.out.println("\n=============");
				System.out.println("Result of Your Input : "
						+ s.getValueIn(row - 1, col - 1));

				System.out.println(s.toString());
				if (s.checkPuzzle() == true) {
					System.out.println("\nThat's Right!!");
					/**
					 * add value to board, this is contemporal
					 * because it set true for boolean 
					 * attribute start[row][col].
					 */
					s.addInitial(row -1, col -1, value);	
				} else if (s.checkPuzzle() == false) {
					System.out.println("\nThat's Wrong!!");
					System.out.println("Resetting...");
					s.reset();
					System.out.println(s.toString());
				}

				
			}
			else{
				System.out.println("That's wrong Input");
			}
		}
	}
/**
 * check wheather input's are right ro wrong.
 */
	private static boolean check_condition(int row, int col, int value) {
		if ((row >= 1 && row <= 9) && (col >= 1 && col <= 9)
				&& (value >= 1 && value <= 9))
			return true;
		else
			return false;
	}
}
