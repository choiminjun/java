
public class Circle {
	double radius;
	double area;
	double diameter;
	public Circle(){
		radius = 0;
		area = 0;
		diameter = 0;
	}
	public void setRadius(double a){
		this.radius = a;
		//this.diameter= 4;
		//this.area = 2.5 * 2.5 * 3.14;
	
	}
	public double getRadius(){
		
		return radius;
	}
	public double computeDiameter(){
		return 2 * radius;
	}
	public double computeArea(){
		this.area = radius * radius * 3.14;
		return area;
	}
}
