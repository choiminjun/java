/**
 Program displays second lexicographic word.
 Author : Choi Min Jun.
 Email Address: choiminjun0720@gmail.com
 Programming Projects 3 - 3.
 Last Changed : 26/03/2014.
 */
import java.util.ArrayList;
import java.util.Scanner;

/**
 * public class StringCompare { public static void main(String[] args){
 * ArrayList<String> str = new ArrayList<String>();
 * System.out.println("Put three words"); Scanner keyboard = new
 * Scanner(System.in);
 * 
 * String temp; for(int i = 0; i < 3; i++){ temp = keyboard.next();
 * str.add(temp); }
 * 
 * //first, find smallest String min = str.get(0); for(int i = 1 ; i <
 * str.size() ; i++){ if(min.compareTo( str.get(i)) < 0 ) min = str.get(i); }
 * str.remove(min); //remove smallest from arraylist.
 * 
 * min = str.get(0); if(min.compareTo(str.get(1)) < 0) //after remove smallest,
 * compare last two strings. min = str.get(1);
 * 
 * System.out.println("The second lexicographic String is " + min);
 * 
 * } }
 */
public class StringCompare {
	//public static String[] str = new String[3];

	public static void main(String[] args) {
		String[] str = new String[3];
		String temp;
		Scanner keyboard = new Scanner(System.in);
		for (int i = 0; i < 3; i++) {
			System.out.println("Put String");
			str[i] = keyboard.nextLine();
		}

		for (int i = 0; i < 3; i++) {
			int temp_index = getIndexOfSmallest(i, str);
			InterChange(temp_index,i, str);
		}
		
		for(int i = 0 ; i < 3; i++){
			System.out.println(str[i]);
		}
		
		System.out.println("Testing...");
		if(str[0].compareTo(str[1]) == 0){
			System.out.println("Yes its ignore");
		}
	}

	private static void InterChange(int temp1,int temp2, String[] str) {
		// TODO Auto-generated method stub
		String temp = str[temp1];
		str[temp1]= str[temp2];
		str[temp2] = temp;

	}

	public static int getIndexOfSmallest(int current_index, String[] arr) {
		int min_index = current_index;
		String min_string = arr[min_index];
		for (int i = min_index + 1; i < 3; i++) {
			if (arr[min_index].compareTo(arr[i]) == 0) {
				min_index = i;
			}
		}
		return min_index;
	}
}
