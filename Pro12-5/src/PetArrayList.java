/**
 * Program:12-4 Sort Using ArrayList.
 * Author:Choi Min
 * Jun Last Changed: 05/06/2014
 * 
 * @author MINJUN
 */
import java.util.ArrayList;
import java.util.Scanner;

public class PetArrayList {
	public static void main(String[] args) {
		ArrayList<Pet> list = new ArrayList<Pet>();
		String check = "y";
		Scanner keyboard = new Scanner(System.in);
		do {
			System.out.println("Put Pets Name, Age, Weight");
			String name = keyboard.next();
			int age = keyboard.nextInt();
			double weight = keyboard.nextDouble();
			Pet newPet = new Pet(name, age, weight); // using constructor.
			list.add(newPet);
			System.out.println("You want to make more Pet Object?");
			check = keyboard.next();
		} while (check.equalsIgnoreCase("y"));
		// call sort method.
		list = sortByWeight(list);
		// display pet info using for-each loop.
		for (Pet x : list)
			System.out.println(x);

		getPercentageOfWeight(list);
	}

	/**
	 * Sort list by using compare method and return arrayList.
	 * 
	 * @param list
	 * @return
	 */
	public static ArrayList<Pet> sortByWeight(ArrayList<Pet> list) {
		for (int i = 0; i < list.size(); i++) {
			for (int j = 0; j < list.size() - 1; j++) {
				if (list.get(j).getWeight() > list.get(j + 1).getWeight()) {
					Pet temp = list.get(j + 1);
					list.set(j + 1, list.get(j));
					list.set(j, temp);
				}
			}
		}
		return list;
	}

	/**
	 * Get Percentage and number of Pets.
	 */
	private static void getPercentageOfWeight(ArrayList<Pet> list) {

		int numUnderFivePounds, numBetweenFivetoTenPounds, numUpperTenPounds;
		numUnderFivePounds = numBetweenFivetoTenPounds = numUpperTenPounds = 0;

		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).getWeight() < 5)
				numUnderFivePounds++;
			else if (list.get(i).getWeight() >= 5
					&& list.get(i).getWeight() <= 10)
				numBetweenFivetoTenPounds++;
			else
				numUpperTenPounds++;

		}
		int tot = numUnderFivePounds + numBetweenFivetoTenPounds + numUpperTenPounds;
		System.out.println("Num,Percent Under 5Pounds : " + numUnderFivePounds + " , " + numUnderFivePounds/(double)tot);
		System.out.println("Num,Percent Between 5Pounds to 10Pounds: " + numBetweenFivetoTenPounds + " , " + numBetweenFivetoTenPounds/(double)tot);
		System.out.println("Num,Percent Upper 10Pounds: " + numUpperTenPounds + " , " + numUpperTenPounds / (double)tot);
	}
}
