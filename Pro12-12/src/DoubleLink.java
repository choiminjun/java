import java.util.Scanner;
public class DoubleLink {	
	private newnode newnode;
	private newnode tail;
	private newnode temp;
	private newnode head;
	public DoubleLink(){
		head = null;
		tail = null;
	}
	public void showList(){
		newnode head_position = head;
		newnode tail_position = tail;
		System.out.println("Printing from head");
		while(head_position != null){
			System.out.print(head_position.data+"   ");
			head_position = head_position.R_next;
		}
		System.out.println();
		System.out.println("Printing from tail");
		while(tail_position != null){
			System.out.print(tail_position.data+ "    ");
			tail_position = tail_position.L_next;;
		}
	}
	
	public void addANodeToStart(String addData){
		
		newnode = new newnode(addData);
		if(head == null){			
			head = newnode;
			head.L_next = null;
			head.R_next = null;
			tail = newnode;
		}
		else{		//make new nodes to link doubly
			temp = tail;		
			tail.R_next = newnode;
			tail = newnode;
			tail.L_next = temp;
			tail.R_next = null;
		}
	}
	public class newnode{
		private String data;
		private newnode L_next;
		private newnode R_next;
		public newnode(){
			data = null;
			L_next = null;
			R_next = null;
		}
		public newnode(String addData){
			this.data = addData;
		}
	}
}

