import org.junit.*;
import static org.junit.Assert.*;

/**
 * The class <code>CalTemperatureTest</code> contains tests for the class <code>{@link CalTemperature}</code>.
 *
 * @generatedBy CodePro at 14. 6. 10 오후 4:28
 * @author MINJUN
 * @version $Revision: 1.0 $
 */
public class CalTemperatureTest {
	/**
	 * Run the CalTemperature() constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:28
	 */
	@Test
	public void testCalTemperature_1()
		throws Exception {

		CalTemperature result = new CalTemperature();

		// add additional test code here
		assertNotNull(result);
		assertEquals(-17.777779f, result.cal_Celsius(), 1.0f);
		assertEquals(32.0f, result.cal_Fahrenheit(), 1.0f);
		assertEquals(0.0f, result.get_temp(), 1.0f);
		assertEquals(' ', result.get_scale());
	}

	/**
	 * Run the CalTemperature(char) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:28
	 */
	@Test
	public void testCalTemperature_2()
		throws Exception {
		char c = '';

		CalTemperature result = new CalTemperature(c);

		// add additional test code here
		assertNotNull(result);
		assertEquals(-17.777779f, result.cal_Celsius(), 1.0f);
		assertEquals(32.0f, result.cal_Fahrenheit(), 1.0f);
		assertEquals(0.0f, result.get_temp(), 1.0f);
		assertEquals('', result.get_scale());
	}

	/**
	 * Run the CalTemperature(float) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:28
	 */
	@Test
	public void testCalTemperature_3()
		throws Exception {
		float v = 1.0f;

		CalTemperature result = new CalTemperature(v);

		// add additional test code here
		assertNotNull(result);
		assertEquals(-17.222221f, result.cal_Celsius(), 1.0f);
		assertEquals(33.8f, result.cal_Fahrenheit(), 1.0f);
		assertEquals(1.0f, result.get_temp(), 1.0f);
		assertEquals(' ', result.get_scale());
	}

	/**
	 * Run the CalTemperature(float,char) constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:28
	 */
	@Test
	public void testCalTemperature_4()
		throws Exception {
		float v = 1.0f;
		char c = '';

		CalTemperature result = new CalTemperature(v, c);

		// add additional test code here
		assertNotNull(result);
		assertEquals(-17.222221f, result.cal_Celsius(), 1.0f);
		assertEquals(33.8f, result.cal_Fahrenheit(), 1.0f);
		assertEquals(1.0f, result.get_temp(), 1.0f);
		assertEquals('', result.get_scale());
	}

	/**
	 * Run the float cal_Celsius() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:28
	 */
	@Test
	public void testCal_Celsius_1()
		throws Exception {
		CalTemperature fixture = new CalTemperature(1.0f, '');

		float result = fixture.cal_Celsius();

		// add additional test code here
		assertEquals(-17.222221f, result, 0.1f);
	}

	/**
	 * Run the float cal_Fahrenheit() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:28
	 */
	@Test
	public void testCal_Fahrenheit_1()
		throws Exception {
		CalTemperature fixture = new CalTemperature(1.0f, '');

		float result = fixture.cal_Fahrenheit();

		// add additional test code here
		assertEquals(33.8f, result, 0.1f);
	}

	/**
	 * Run the int compare(CalTemperature,CalTemperature) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:28
	 */
	@Test
	public void testCompare_1()
		throws Exception {
		CalTemperature A = new CalTemperature(1.0f, '');
		CalTemperature B = new CalTemperature(1.0f, '');

		int result = CalTemperature.compare(A, B);

		// add additional test code here
		assertEquals(-2, result);
	}

	/**
	 * Run the int compare(CalTemperature,CalTemperature) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:28
	 */
	@Test
	public void testCompare_2()
		throws Exception {
		CalTemperature A = new CalTemperature(1.0f, 'C');
		CalTemperature B = new CalTemperature(1.0f, 'C');

		int result = CalTemperature.compare(A, B);

		// add additional test code here
		assertEquals(-2, result);
	}

	/**
	 * Run the int compare(CalTemperature,CalTemperature) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:28
	 */
	@Test
	public void testCompare_3()
		throws Exception {
		CalTemperature A = new CalTemperature(1.0f, 'c');
		CalTemperature B = new CalTemperature(1.0f, 'c');

		int result = CalTemperature.compare(A, B);

		// add additional test code here
		assertEquals(-2, result);
	}

	/**
	 * Run the int compare(CalTemperature,CalTemperature) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:28
	 */
	@Test
	public void testCompare_4()
		throws Exception {
		CalTemperature A = new CalTemperature(1.0f, 'C');
		CalTemperature B = new CalTemperature(1.0f, 'C');

		int result = CalTemperature.compare(A, B);

		// add additional test code here
		assertEquals(-2, result);
	}

	/**
	 * Run the int compare(CalTemperature,CalTemperature) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:28
	 */
	@Test
	public void testCompare_5()
		throws Exception {
		CalTemperature A = new CalTemperature(1.0f, 'F');
		CalTemperature B = new CalTemperature(1.0f, 'F');

		int result = CalTemperature.compare(A, B);

		// add additional test code here
		assertEquals(-2, result);
	}

	/**
	 * Run the int compare(CalTemperature,CalTemperature) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:28
	 */
	@Test
	public void testCompare_6()
		throws Exception {
		CalTemperature A = new CalTemperature(1.0f, 'F');
		CalTemperature B = new CalTemperature(1.0f, 'F');

		int result = CalTemperature.compare(A, B);

		// add additional test code here
		assertEquals(-2, result);
	}

	/**
	 * Run the int compare(CalTemperature,CalTemperature) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:28
	 */
	@Test
	public void testCompare_7()
		throws Exception {
		CalTemperature A = new CalTemperature(1.0f, '');
		CalTemperature B = new CalTemperature(1.0f, '');

		int result = CalTemperature.compare(A, B);

		// add additional test code here
		assertEquals(-2, result);
	}

	/**
	 * Run the char get_scale() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:28
	 */
	@Test
	public void testGet_scale_1()
		throws Exception {
		CalTemperature fixture = new CalTemperature(1.0f, '');

		char result = fixture.get_scale();

		// add additional test code here
		assertEquals('', result);
	}

	/**
	 * Run the float get_temp() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:28
	 */
	@Test
	public void testGet_temp_1()
		throws Exception {
		CalTemperature fixture = new CalTemperature(1.0f, '');

		float result = fixture.get_temp();

		// add additional test code here
		assertEquals(1.0f, result, 0.1f);
	}

	/**
	 * Run the void set_method(char) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:28
	 */
	@Test
	public void testSet_method_1()
		throws Exception {
		CalTemperature fixture = new CalTemperature(1.0f, '');
		char c = '';

		fixture.set_method(c);

		// add additional test code here
	}

	/**
	 * Run the void set_method(float) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:28
	 */
	@Test
	public void testSet_method_2()
		throws Exception {
		CalTemperature fixture = new CalTemperature(1.0f, '');
		float v = 1.0f;

		fixture.set_method(v);

		// add additional test code here
	}

	/**
	 * Run the void set_method(float,char) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:28
	 */
	@Test
	public void testSet_method_3()
		throws Exception {
		CalTemperature fixture = new CalTemperature(1.0f, '');
		float v = 1.0f;
		char c = '';

		fixture.set_method(v, c);

		// add additional test code here
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:28
	 */
	@Before
	public void setUp()
		throws Exception {
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:28
	 */
	@After
	public void tearDown()
		throws Exception {
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 14. 6. 10 오후 4:28
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(CalTemperatureTest.class);
	}
}