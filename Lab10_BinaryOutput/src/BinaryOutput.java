import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

public class BinaryOutput {
	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		DataOutputStream dos;
		int num = 1;
		try {
			dos = new DataOutputStream(new FileOutputStream("numbers.dat"));
			System.out.println("Enter nonnegative integers");
			System.out.println("Place a negative number at the end");
			while (num > 0) {
				num = keyboard.nextInt();
				dos.writeInt(num);
			}
			dos.close();
		} catch (IOException e) {
			System.out.println("Problem with output...");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
