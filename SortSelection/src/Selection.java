
public class Selection {
	
	public int[] sort(int[] arr){
		int min , min_index;
		//int sorted_arr[] = new int[20];
		for(int i = 0; i < arr.length-1; i++){
			min = arr[i];
			min_index = i;
			for(int j = i+1; j < arr.length; j++){
				if(min > arr[j]){
					min = arr[j];
					min_index = j;
				}
			}
			int temp = arr[i];
			arr[i] = min;
			arr[min_index] = temp;
		}
		
		return arr;
	}
	public int[] setValue(int[] arr){
		for(int i = 0; i < arr.length; i++)
			arr[i] = (int)(Math.random()*21);
		return arr;
	}
}
