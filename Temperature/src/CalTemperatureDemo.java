/**
 * Program testing using constructor, set,get and compare methods. Author : Choi
 * Min Jun. Email Address: choiminjun0720@gmail.com LAB Last Changed :
 * 16/04/2014.
 */
public class CalTemperatureDemo {
	public static void main(String[] args) {
		CalTemperature test01 = new CalTemperature();
		CalTemperature test02 = new CalTemperature('c');
		CalTemperature test03 = new CalTemperature((float) 100);
		CalTemperature test04 = new CalTemperature((float) 32, 'f');

		// test constuctor.
		System.out.println("TEST 4 Constructors..");
		System.out.print(test01.get_temp() + "	" + test01.get_scale() + "\n");
		System.out.print(test02.get_temp() + "	" + test02.get_scale() + "\n");
		System.out.print(test03.get_temp() + "	" + test03.get_scale() + "\n");
		System.out.print(test04.get_temp() + "	" + test04.get_scale() + "\n");
		System.out.println("\n");

		// test three setting method
		test01.set_method('c');
		test02.set_method((float) 32.0, 'F');
		test03.set_method(5000);
		System.out.println("Test three setting method.");
		System.out.print(test01.get_temp() + "	" + test01.get_scale() + "\n");
		System.out.print(test02.get_temp() + "	" + test02.get_scale() + "\n");
		System.out.print(test03.get_temp() + "	" + test03.get_scale() + "\n");
		System.out.print(test04.get_temp() + "	" + test04.get_scale() + "\n");

		// test two calculate method using test01 and test02.
		System.out.println("\n\nTest two calculate method....");
		System.out.println("Before Test01 Calculate : " + test01.get_temp()
				+ test01.get_scale());
		System.out.println("Before Test02 Calculate : " + test02.get_temp()
				+ test02.get_scale());
		System.out.println("After Test01 Calculate : "
				+ test01.cal_Fahrenheit() + test01.get_scale());
		System.out.println("After Test02 Calculate : " + test02.cal_Celsius()
				+ test02.get_scale());

		// Test compare method..using test01 and test02.
		System.out.println("\n\nTest two Compare method....");
		int compare_Result = CalTemperature.compare(test01, test02);
		if (compare_Result == 1) {
			System.out.println("Test01 is bigger.");
		} else if (compare_Result == -1) {
			System.out.println("Test02 is bigger.");
		} else if (compare_Result == 0) {
			System.out.println("Test01 and Test02 is same!");
		} else {
			System.out.println("Error!!");
		}
		System.out.println("Before Compare Test01 and Test02 : "
				+ test01.get_temp() + test01.get_scale());
		System.out.println("Before Test02 Calculate : " + test02.get_temp()
				+ test02.get_scale());
		System.out.println("After Test01 Calculate : "
				+ test01.cal_Fahrenheit() + test01.get_scale());
		System.out.println("After Test02 Calculate : " + test02.cal_Celsius()
				+ test02.get_scale());

	}

}
