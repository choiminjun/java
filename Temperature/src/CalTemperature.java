public class CalTemperature {
	private float temperature;
	private char scale;

	/**
	 * four constructors using different parameters and types.
	 */
	public CalTemperature() {
		this.temperature = 0;
		this.scale = 0;
	}

	public CalTemperature(float v) {
		this.temperature = v;
	}

	public CalTemperature(char c) {
		this.scale = c;
	}

	public CalTemperature(float v, char c) {
		this.temperature = v;
		this.scale = c;
	}

	/**
	 * Calculate celsius and Fahrenheit using temperature value.
	 * 
	 * @param Farenheit
	 * @return
	 */
	public float cal_Celsius() {
		float Fahrenheit = this.temperature;
		float new_Celsius = (float) (5 * ((Fahrenheit - 32.0) / 9));
		return new_Celsius;
	}

	public float cal_Fahrenheit() {
		float Celsius = this.temperature;
		float new_Farenheit = (float) (((9 * Celsius) / 5.0) + 32);
		return new_Farenheit;
	}

	/**
	 * By using two object parameters, compare wheather each objects temperature
	 * values are equal, bigger and smaller.
	 */
	public static int compare(CalTemperature A, CalTemperature B) {
		if (A.scale == B.scale) {
			return -2;
		} else { // when comparing case.(when program runs this loop, it means
					// they have different scales.
			if (A.scale == 'c' || A.scale == 'C') {
				float Object_A_farenheit = A.cal_Fahrenheit();
				float Object_B_farenheit = B.cal_Fahrenheit();
				if (Object_A_farenheit > Object_B_farenheit)
					return 1;
				else if (Object_A_farenheit < Object_B_farenheit)
					return -1;
				else
					return 0;
			} else if (A.scale == 'f' || A.scale == 'F') {
				float Object_A_celsius = A.cal_Celsius();
				float Object_B_celsius = B.cal_Celsius();
				if (Object_A_celsius > Object_B_celsius)
					return 1;
				else if (Object_A_celsius < Object_B_celsius)
					return -1;
				else
					return 0;
			} else
				return -2;
		}
	}

	/**
	 * seeting method using overloading.
	 * 
	 * @param v
	 */
	public void set_method(float v) {
		this.temperature = v;
	}

	public void set_method(char c) {
		this.scale = c;
	}

	public void set_method(float v, char c) {
		this.temperature = v;
		this.scale = c;
	}

	/**
	 * get method.
	 * 
	 * @return
	 */
	public float get_temp() {
		return this.temperature;
	}

	public char get_scale() {
		return this.scale;
	}

}
