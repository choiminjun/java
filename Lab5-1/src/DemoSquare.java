
public class DemoSquare {
	public static void main(String[] args){
		Square s = new Square();
		s.setHeight(20.5);
		s.setWidth(10.5);
		System.out.println("The Area is " + s.computeArea());
	}
}
