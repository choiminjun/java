import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;


public class CollectStream {
	public static byte[] samples;
	public static void main(String[] args) throws UnsupportedAudioFileException, IOException{
		File Female01 = new File("female-01.wav");
		File Female02 = new File("female-02.wav");
		File Female03 = new File("female-03.wav");
		File male01 = new File("male-01.wav");
		File male02 = new File("male-02.wav");
		File male03 = new File("male-03.wav");

		AudioInputStream is = AudioSystem.getAudioInputStream(Female01);
		DataInputStream dis = new DataInputStream(is);      //So we can use readFully()
		try
		{
		    AudioFormat format = is.getFormat();
		    samples = new byte[(int)(is.getFrameLength() * format.getFrameSize())];
		    dis.readFully(samples);
		}
		finally
		{
			//print streming data.
			for(int i = 0 ; i < samples.length; i++)
				System.out.print(samples[i] + " ");
		    dis.close();
		}
	}
}
